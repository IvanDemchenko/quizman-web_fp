﻿using System.Web.Optimization;

namespace QuizMan.WEB
{
    public static class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/style.css"));
            bundles.Add(new ScriptBundle("~/bootstrap/js").Include(
                "~/Scripts/bootstrap.bundle.js"));


            bundles.Add(new ScriptBundle("~/quizman/test-editor/js").Include(
                "~/Scripts/app_scripts/quizman.test.editor.js"));
        }
    }
}
