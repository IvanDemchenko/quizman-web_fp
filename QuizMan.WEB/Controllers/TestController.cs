﻿using Microsoft.AspNet.Identity;
using PagedList;
using QuizMan.BLL.DataTransferObjects;
using QuizMan.BLL.DataTransferObjects.AnswerModel;
using QuizMan.BLL.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using QuizMan.WEB.Models;
using QuizMan.WEB.Utils;

namespace QuizMan.WEB.Controllers
{
    /// <summary>
    /// Represents the test controller
    /// </summary>
    public class TestController : BaseController
    {
        private readonly ITestService _testService;
        private readonly ICategoryService _categoryService;

        /// <summary>
        /// Initializes an instance of the test controller
        /// </summary>
        /// <param name="loggerService">Logger service</param>
        /// <param name="testService">Test service</param>
        /// <param name="categoryService">Category service</param>
        public TestController(ILoggerService loggerService,ITestService testService, ICategoryService categoryService):base(loggerService)
        {
            _testService = testService;
            _categoryService = categoryService;
        }

        /// <summary>
        /// Returns tests by query and category
        /// </summary>
        /// <param name="query">Search query</param>
        /// <param name="categoryId">Category primary key</param>
        /// <returns></returns>
        public async Task<ActionResult> Index(string query, int? categoryId)
        {
            if (query != null && query.Length > 50)
                ModelState.AddModelError("query", "Search string must be no more than 50 characters.");
            ViewBag.Query = query;
            if (categoryId.HasValue)
            {
                ViewBag.CategoryId = categoryId;
                var category = await _categoryService.GetCategoryByIdAsync(categoryId.Value);
                var model = new TestsViewModel
                {
                    Title = category.Title,
                    Description = category.Description
                };
                return View(model);
            }
            if (ModelState.IsValid) return View();
            TempData["ViewData"] = ViewData;
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Returns tests from database
        /// </summary>
        /// <param name="page">Page number</param>
        /// <param name="query">Search query</param>
        /// <param name="currentFilter"></param>
        /// <param name="categoryId">Category primary key</param>
        /// <returns></returns>
        public ActionResult GetTests(int? page, string query, string currentFilter, int? categoryId)
        {
            if (query != null)
                page = 1;
            else
                query = currentFilter;

            ViewBag.CurrentFilter = query;
            ViewBag.CategoryId = categoryId;

            var model = User.Identity.IsAuthenticated ?
                _testService.GetTestsWithProgress(User.Identity.GetUserId(),categoryId).Where(x => !x.Deprecated && x.IsActive).Reverse() :
                _testService.GetTests(categoryId).Where(x => !x.Deprecated && x.IsActive).Reverse();

            foreach (var test in model.Where(test => test.Description.Length > 100))
                test.Description = test.Description.Substring(0, 100) + "...";

            if (!string.IsNullOrEmpty(query))
            {
                if (query.Length > 50) 
                    ModelState.AddModelError("", "Search string must be no more than 50 characters.");
                else
                    model = model.Where(s => s.Title.CaseInsensitiveContains(query));
            }
               
            var pageSize = 9;
            var pageNumber = (page ?? 1);

            return PartialView("TestsPartial", model.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Returns user tests results 
        /// </summary>
        /// <param name="page">Page number</param>
        /// <param name="query">Search query</param>
        /// <param name="currentFilter">Current search filter</param>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        public ActionResult GetUserTestResults(int? page, string query, string currentFilter, string userId)
        {
            if (query != null)
                page = 1;
            else
                query = currentFilter;
            ViewBag.CurrentFilter = query;
            ViewBag.UserId = userId;
            var model = _testService.GetTestQuickResultsByUserId(userId);
            if (!string.IsNullOrEmpty(query))
            {
                if (query.Length > 50)
                    ModelState.AddModelError("", "Search string must be no more than 50 characters.");
                else
                    model = model.Where(s => s.Title.CaseInsensitiveContains(query));
            }
            var pageSize = 9;
            var pageNumber = (page ?? 1);
            return PartialView("QuickResultsPartial", model.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Returns tests by category
        /// </summary>
        /// <param name="page">Page number</param>
        /// <param name="query">Search query</param>
        /// <param name="currentFilter">Current search filter</param>
        /// <param name="categoryId">Category primary key</param>
        /// <returns></returns>
        public ActionResult GetCategoryTests(int? page, string query, string currentFilter, int categoryId)
        {
            if (query != null)
                page = 1;
            else
                query = currentFilter;
            ViewBag.CurrentFilter = query;
            ViewBag.CategoryId = categoryId;
            var model = _testService.GetTestsByCategoryId(categoryId).Reverse();
            if (!string.IsNullOrEmpty(query))
                model = model.Where(s => s.Title.CaseInsensitiveContains(query));

            var pageSize = 5;
            var pageNumber = (page ?? 1);

            return PartialView("CategoryTestsPartial", model.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Return test information
        /// </summary>
        /// <param name="testId">Test primary key</param>
        /// <returns></returns>
        public async Task<ActionResult> TestPreview(int testId)
        {
            var model = User.Identity.IsAuthenticated ?
                await _testService.GetTestByIdWithProgress(testId, User.Identity.GetUserId()) :
                await _testService.GetTestById(testId);
            return View(model);
        }

        /// <summary>
        /// The action of the beginning of the test.  
        /// </summary>
        /// <param name="testId">Test primary key</param>
        /// <returns></returns>
        [Authorize]
        public async Task<ActionResult> StartTest(int testId)
        {
            ViewBag.EndTime = await _testService.RegisterUserToTest(User.Identity.GetUserId(), testId);
            var test = await _testService.GetTestWithDetailsByIdAsync(testId);
            return View(test);
        }

        /// <summary>
        /// The post action of the beginning of the test. 
        /// </summary>
        /// <param name="testAnswersModel">User answer model</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public async Task<ActionResult> StartTest(TestAnswersModel testAnswersModel)
        {
            await _testService.AddUserAnswer(testAnswersModel, User.Identity.GetUserId());
            return RedirectToAction("GetQuickResult", new { testId = testAnswersModel.TestId });
        }

        /// <summary>
        /// Returns test quick result
        /// </summary>
        /// <param name="testId">Test primary key</param>
        /// <returns></returns>
        [Authorize]
        public async Task<ActionResult> GetQuickResult(int testId)
        {
            var model = await _testService.GetQuickResult(testId, User.Identity.GetUserId());
            return View(model);
        }

        /// <summary>
        /// Returns detailed test result
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <param name="testId">Test primary key</param>
        /// <returns></returns>
        [Authorize(Roles = Role.Admin)]
        public async Task<ActionResult> GetDetailedResult(string userId, int testId)
        {
            var model = await _testService.GetDetailedResult(testId, userId);
            return View(model);
        }

        /// <summary>
        /// Action for editing test
        /// </summary>
        /// <param name="testId">Test primary key</param>
        /// <returns></returns>
        [AuthorizeRoles(Role.Admin, Role.Editor)]
        public async Task<ActionResult> EditTest(int testId)
        {
            var model = await _testService.GetTestWithDetailsByIdAsync(testId);
            ViewBag.Categories = new SelectList(_categoryService.GetAllCategoriesWithShortDescription(), "Id", "Title", model.CategoryId);
            return View(model);
        }

        /// <summary>
        /// Post action for editing test 
        /// </summary>
        /// <param name="testModel">Test model</param>
        /// <returns></returns>
        [AuthorizeRoles(Role.Admin, Role.Editor)]
        [HttpPost]
        public async Task<ActionResult> EditTest(TestModel testModel)
        {
            ModelState.Merge(_testService.ValidateTestModel(testModel),"");
            if (!ModelState.IsValid)
            {
                ViewBag.Categories = new SelectList(_categoryService.GetAllCategoriesWithShortDescription(), "Id", "Title", testModel.CategoryId);
                return View(testModel);
            }
            await _testService.AddOrUpdateTest(testModel);
            return RedirectToAction("EditCategory", "Category", new { categoryId = testModel.CategoryId });
        }

        /// <summary>
        /// Action for deleting test
        /// </summary>
        /// <param name="testId">Test primary key</param>
        /// <returns></returns>
        [AuthorizeRoles(Role.Admin, Role.Editor)]
        public async Task<JsonResult> DeleteTest(int testId)
        {
            try
            {
                await _testService.DeleteTestById(testId);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "Test not deleted! Ex:" + ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, responseText = "Test successfully deleted" }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Action for adding test
        /// </summary>
        /// <param name="categoryId">Category primary key</param>
        /// <returns></returns>
        [AuthorizeRoles(Role.Admin, Role.Editor)]
        [HttpGet]
        public ActionResult AddTest(int categoryId)
        {
            var testModel = new TestModel { CategoryId = categoryId, TestPassingPercentage = 90, TimeToPass = 30};
            ViewBag.Categories = new SelectList(_categoryService.GetAllCategoriesWithShortDescription(), "Id", "Title", testModel.CategoryId);

            return View("EditTest", testModel);
        }

        /// <summary>
        /// Changes test status (active / not active)
        /// </summary>
        /// <param name="testId">Test primary key</param>
        /// <returns></returns>
        [AuthorizeRoles(Role.Admin, Role.Editor)]
        public async Task<JsonResult> ChangeTestStatus(int testId)
        {
            try
            {
                await _testService.ChangeTestStatus(testId);
            }
            catch (Exception)
            {
                return Json(new { success = false, responseText = "Test status didn`t change" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, responseText = "Test status successfully changed" }, JsonRequestBehavior.AllowGet);
        }
    }
}