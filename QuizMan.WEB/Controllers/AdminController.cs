﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using PagedList;
using QuizMan.BLL.DataTransferObjects.UserManage;
using QuizMan.BLL.Infrastructure;
using QuizMan.BLL.Interfaces;
using QuizMan.WEB.Models;
using QuizMan.WEB.Utils;

namespace QuizMan.WEB.Controllers
{
    /// <summary>
    /// Represents the admin controller
    /// </summary>
    [Authorize(Roles = Role.Admin)]
    public class AdminController : BaseController
    {
        private readonly IUserService _userService;

        /// <summary>
        /// Initializes an instance of the admin controller
        /// </summary>
        /// <param name="loggerService">Logger service</param>
        /// <param name="userService">User service</param>
        public AdminController(ILoggerService loggerService,IUserService userService):base(loggerService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Admin panel action
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Logs page
        /// </summary>
        /// <param name="page">Page number</param>
        /// <returns></returns>
        public ActionResult Logs(int? page)
        {
            var logs = LoggerService.GetAllLogs();
            var pageSize = 10;
            var pageNumber = (page ?? 1);
            return View(logs.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Action for testing logging
        /// </summary>
        /// <returns></returns>
        public JsonResult TestLogging()
        {
            int[] mas = new int[2];
            mas[6] = 4;
            return Json("Ok",JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Users list action
        /// </summary>
        /// <param name="page">Page number</param>
        /// <param name="query">Search query </param>
        /// <param name="currentFilter">Current search query</param>
        /// <returns></returns>
        public async Task<ActionResult> Users(int? page, string query, string currentFilter)
        {
            if (query != null)
                page = 1;
            else
                query = currentFilter;
            ViewBag.CurrentFilter = query;
            var users = _userService.GetAllUsers();
            if (!string.IsNullOrEmpty(query))
            {
                if (query.Length > 50)
                    ModelState.AddModelError("", "Search string must be no more than 50 characters.");
                else
                    users = users.Where(s => (s.FirstName + s.Surname + s.Email).CaseInsensitiveContains(string.Concat(query.Where(c => !char.IsWhiteSpace(c)))));
            }
            var usersRolesViewModel = new List<UserRolesModel>();
            foreach (var user in users)
            {
                var userRolesViewModel = new UserRolesModel
                {
                    Id = user.Id,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    Surname = user.Surname,
                    Git = user.Git,
                    Year = user.Age,
                    Roles = await _userService.GetUserRoles(user.Id),
                    IsBanned = await _userService.GetLockoutEndDateAsync(user.Id) > DateTimeOffset.UtcNow
                };
                usersRolesViewModel.Add(userRolesViewModel);
            }
            var pageSize = 10;
            var pageNumber = (page ?? 1);
            return View(usersRolesViewModel.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Action for editing user roles
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        public async Task<ActionResult> EditUserRoles(string userId)
        {
            var user = await _userService.GetByIdAsync(userId);
            if (user == null)
                throw new ValidationException("", "User not found");

            var model = new EditUserRolesModel
            {
                UserId = user.Id, UserName = user.FirstName, UserSurname = user.Surname, SelectedRoles = new List<SelectedRolesModel>()
            };
            foreach (var role in _userService.GetAllRoles())
            {
                var userRolesViewModel = new SelectedRolesModel
                {
                    RoleId = role.Id,
                    RoleName = role.Name
                };
                if (await _userService.UserIsInRole(user.Id, role.Name))
                    userRolesViewModel.Selected = true;
                else
                    userRolesViewModel.Selected = false;
                model.SelectedRoles.Add(userRolesViewModel);
            }

            return View(model);
        }

        /// <summary>
        /// POST action for editing user roles
        /// </summary>
        /// <param name="editUserRolesModel">User role model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> EditUserRoles(EditUserRolesModel editUserRolesModel)
        {
            await _userService.UpdateUserRoles(editUserRolesModel);
            return RedirectToAction("Users");
        }

        /// <summary>
        /// Action for user banning
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <param name="days">Number of days of lockout</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> LockOutUser(string userId, int days)
        {
            var result = await _userService.SetLockOutDate(userId, days);
            return Json(new { success = result.Succeeded, responseText = result.Message }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Action for user unbanning
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> UnBanUser(string userId)
        {
            var result = await _userService.UnBan(userId);
            return Json(new { success = result.Succeeded, responseText = result.Message }, JsonRequestBehavior.AllowGet);
        }
    }
}