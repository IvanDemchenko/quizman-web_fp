﻿using System;
using System.Web.Mvc;
using QuizMan.BLL.DataTransferObjects;
using QuizMan.BLL.Infrastructure;
using QuizMan.BLL.Interfaces;
using QuizMan.WEB.Models;

namespace QuizMan.WEB.Controllers
{
    /// <summary>
    /// Represent the base controller
    /// </summary>
    public class BaseController : Controller
    {
        protected readonly ILoggerService LoggerService;

        /// <summary>
        /// Initializes an instance of the base controller
        /// </summary>
        /// <param name="loggerService">Logger service</param>
        public BaseController(ILoggerService loggerService)
        {
            LoggerService = loggerService;
        }

        /// <summary>
        /// Handles exceptions and writes to database
        /// </summary>
        /// <param name="filterContext">Filter context</param>
        protected override void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled && filterContext.Exception is ValidationException validationExceptionexception)
            {
                filterContext.ExceptionHandled = true;
                filterContext.Result = RedirectToAction("ValidationError", "Error",
                    new ErrorModel {UserMessage = validationExceptionexception.UserMessage});
            }
            else if (!filterContext.ExceptionHandled && filterContext.Exception is TestException testException)
            {
                filterContext.ExceptionHandled = true;
                filterContext.Result = RedirectToAction("ValidationError", "Error",
                    new ErrorModel { UserMessage = testException.Message });
            }
            else
            {
                var exceptionModel = new ExceptionDetailModel
                {
                    ExceptionMessage = filterContext.Exception.Message,
                    StackTrace = filterContext.Exception.StackTrace,
                    ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                    ActionName = filterContext.RouteData.Values["action"].ToString(),
                    Date = DateTime.Now
                };
                LoggerService.WriteLog(exceptionModel);
                filterContext.ExceptionHandled = true;
                filterContext.Result = RedirectToAction("ValidationError", "Error",
                    new ErrorModel { UserMessage = "Something went wrong :(" });
            }
        }
    }
}