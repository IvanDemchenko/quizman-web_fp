﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using QuizMan.BLL.DataTransferObjects;
using QuizMan.BLL.Interfaces;
using QuizMan.WEB.Models;

namespace QuizMan.WEB.Controllers
{
    /// <summary>
    /// Represents the account controller
    /// </summary>
    public class AccountController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IAuthenticationManager _authenticationManager;

        /// <summary>
        /// Initializes an instance of the account controller
        /// </summary>
        /// <param name="userService">User Service</param>
        /// <param name="authenticationManager">Identity authentication manager</param>
        /// <param name="loggerService">Logger Service</param>
        public AccountController(IUserService userService, IAuthenticationManager authenticationManager, ILoggerService loggerService):base(loggerService)
        {
            _userService = userService;
            _authenticationManager = authenticationManager;
        }

        /// <summary>
        /// Login action 
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// Authentication action
        /// </summary>
        /// <param name="model">Login model</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model)
        {
            if (!ModelState.IsValid) return View(model);
            var userModel = new UserModel{Email = model.Email, Password = model.Password};
            var claim = await _userService.Authenticate(userModel);
            if (claim is null)
            {
                ModelState.AddModelError("","Incorrect login ot password");
            }
            else
            {
                _authenticationManager.SignOut();
                if (await _userService.IsLockedOutAsync(claim.GetUserId()))
                {
                    var lockOutEnd = await _userService.GetLockoutEndDateAsync(claim.GetUserId());
                    if (lockOutEnd > DateTimeOffset.UtcNow)
                    {
                        ViewBag.EndLockOut = await _userService.GetLockoutEndDateAsync(claim.GetUserId());
                        return View("LockOut");
                    }
                }
                _authenticationManager.SignIn(new AuthenticationProperties
                {
                    IsPersistent = true
                },claim);
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        /// <summary>
        /// Log out action
        /// </summary>
        /// <returns></returns>
        public ActionResult LogOut()
        {
            _authenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// User register action
        /// </summary>
        /// <returns></returns>
        public ActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// Register post action
        /// </summary>
        /// <param name="model">Register model</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            if (!ModelState.IsValid) return View(model);
            var userModel = new UserModel
            {
                Email = model.Email,
                Password = model.Password,
                Surname = model.Surname,
                Name = model.Name,
                Role = "user",
                Age = model.Age
            };
            var operationDetails = await _userService.Create(userModel);
            if (operationDetails.Succeeded)
                return View("SuccessRegister");
            else
                ModelState.AddModelError(operationDetails.Property,operationDetails.Message);
            return View(model);
        }

        /// <summary>
        /// User profile action
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        public async Task<ActionResult> Detail(string userId)
        {
            var user =await _userService.GetByIdAsync(userId);
            return View(user);
        }
        
        /// <summary>
        /// User update profile action
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public async Task<ActionResult> UpdateProfile()
        {
            var user =await _userService.GetByIdAsync(User.Identity.GetUserId());
            return View(user);
        }

        /// <summary>
        /// User update profile post action
        /// </summary>
        /// <param name="applicationUserModel">Application user model</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> UpdateProfile(ApplicationUserModel applicationUserModel)
        {
            applicationUserModel.Id = User.Identity.GetUserId();
            await _userService.UpdateUserProfile(applicationUserModel);
            return RedirectToAction("Detail", new { userId = applicationUserModel.Id});
        }

        /// <summary>
        /// Change password action
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        /// <summary>
        /// Change password post action
        /// </summary>
        /// <param name="changePasswordModel">Change password model</param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> ChangePassword(ChangePasswordModel changePasswordModel)
        {
            changePasswordModel.UserId = User.Identity.GetUserId();
            await _userService.ChangePassword(changePasswordModel);
            return RedirectToAction("Detail", new { userId = User.Identity.GetUserId()});
        }
    }
}