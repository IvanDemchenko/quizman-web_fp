﻿using System.Web.Mvc;
using QuizMan.WEB.Models;

namespace QuizMan.WEB.Controllers
{
    /// <summary>
    /// Represents the error controller  
    /// </summary>
    public class ErrorController:Controller
    {
        /// <summary>
        /// Returns validation error page
        /// </summary>
        /// <param name="error">Error model</param>
        /// <returns></returns>
        public ActionResult ValidationError(ErrorModel error)
        {
            return View(error);
        }

        public ActionResult NotFound()
        {
            Response.StatusCode = 404;
            return View();
        }
    }
}