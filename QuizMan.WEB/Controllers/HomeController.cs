﻿using System.Linq;
using System.Web.Mvc;
using QuizMan.BLL.Interfaces;

namespace QuizMan.WEB.Controllers
{
    /// <summary>
    /// Represents the home controller
    /// </summary>
    public class HomeController : Controller
    {
        private readonly ITestService _testService;
        private readonly ICategoryService _categoryService;
        private readonly IUserService _userService;

        /// <summary>
        /// Initializes an instance of the home controller
        /// </summary>
        /// <param name="testService">Test service</param>
        /// <param name="categoryService">Category service</param>
        /// <param name="userService">User service</param>
        public HomeController( ITestService testService, ICategoryService categoryService, IUserService userService)
        {
            _testService = testService;
            _categoryService = categoryService;
            _userService = userService;
        }

        /// <summary>
        /// Return main application page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (TempData["ViewData"] != null)
                ViewData = (ViewDataDictionary)TempData["ViewData"];
            var model = _testService.GetTopUsers().ToList();
            ViewBag.UserCount = _userService.UsersNumber();
            ViewBag.TestCount = _testService.TestsNumber();
            ViewBag.CategoryCount = _categoryService.CategoriesNumber();
            return View(model);
        }

        /// <summary>
        /// About page
        /// </summary>
        /// <returns></returns>
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        /// <summary>
        /// Contact page
        /// </summary>
        /// <returns></returns>
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
    }
}