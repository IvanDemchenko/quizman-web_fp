﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using PagedList;
using QuizMan.BLL.DataTransferObjects;
using QuizMan.BLL.Interfaces;
using QuizMan.WEB.Models;
using QuizMan.WEB.Utils;

namespace QuizMan.WEB.Controllers
{
    /// <summary>
    /// Represents the category controller 
    /// </summary>
    public class CategoryController : BaseController
    {
        private readonly ICategoryService _categoryService;

        /// <summary>
        /// Initializes an instance of the category controller
        /// </summary>
        /// <param name="loggerService">Logger service</param>
        /// <param name="categoryService">Category service</param>
        public CategoryController(ILoggerService loggerService, ICategoryService categoryService):base(loggerService)
        {
            _categoryService = categoryService;
        }

        /// <summary>
        /// Returns categories
        /// </summary>
        /// <param name="page">Page number</param>
        /// <param name="query">Search query</param>
        /// <param name="currentFilter">Current search query</param>
        /// <returns></returns>
        public ActionResult GetCategories(int? page, string query, string currentFilter)
        {
            if (query != null)
                page = 1;
            else
                query = currentFilter;
            ViewBag.CurrentFilter = query;
            var model = _categoryService.GetAllCategoriesWithShortDescription();
            if (!string.IsNullOrEmpty(query))
            {
                if (query.Length > 50)
                    ModelState.AddModelError("", "Search string must be no more than 50 characters.");
                else
                    model = model.Where(s => s.Title.CaseInsensitiveContains(query));
            }
            var pageSize = 9;
            var pageNumber = (page ?? 1);
            return PartialView("CategoriesPartial", model.ToPagedList(pageNumber, pageSize));
        }

        /// <summary>
        /// Adds category
        /// </summary>
        /// <returns></returns>
        [AuthorizeRoles(Role.Admin,Role.Editor)]
        public ActionResult AddCategory()
        {
            var category = new CategoryModel();
            return View("EditCategory", category);
        }

        /// <summary>
        /// Edits category
        /// </summary>
        /// <param name="categoryId">Category primary key</param>
        /// <returns></returns>
        [AuthorizeRoles(Role.Admin, Role.Editor)]
        public async Task<ActionResult> EditCategory(int categoryId)
        {
            var category = await _categoryService.GetCategoryWithDetailsByIdAsync(categoryId);
            return View(category);
        }

        /// <summary>
        /// Adds or updates category
        /// </summary>
        /// <param name="categoryModel">Category model</param>
        /// <returns></returns>
        [AuthorizeRoles(Role.Admin, Role.Editor)]
        public async Task<JsonResult> AddOrUpdateCategory(CategoryModel categoryModel)
        {
            int id;
            try
            {
                var errors = _categoryService.ValidateCategory(categoryModel);
                var errorsModel = errors.Select(error => new ErrorModel() {UserMessage = error.Value}).ToList();
                if (errors.Count == 0)
                    id = await _categoryService.AddOrUpdateCategory(categoryModel);
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return Json(errorsModel, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception)
            {
                return Json(new { success = false, responseText = categoryModel.Title + " did`nt save" });
            }
            return Json(new { success = true, responseText = categoryModel.Title + " successfully saved", categoryId = id });
        }

        /// <summary>
        /// Deletes category
        /// </summary>
        /// <param name="categoryId">Category primary key</param>
        /// <returns></returns>
        [AuthorizeRoles(Role.Admin, Role.Editor)]
        public async Task<ActionResult> DeleteCategory(int categoryId)
        {
            await _categoryService.DeleteCategory(categoryId);
            return RedirectToAction("Index", "Home");
        }

    }
}