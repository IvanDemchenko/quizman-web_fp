﻿using System.Web.Mvc;

namespace QuizMan.WEB.Utils
{
    /// <summary>
    /// Attribute for joining roles
    /// </summary>
    public class AuthorizeRolesAttribute:AuthorizeAttribute
    {
        public AuthorizeRolesAttribute(params string[] roles)
        {
            Roles = string.Join(",", roles);
        }
    }
}