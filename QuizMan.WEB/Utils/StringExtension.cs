﻿using System;

namespace QuizMan.WEB.Utils
{
    /// <summary>
    /// String extension
    /// </summary>
    public static class StringExtension
    {
        /// <summary>
        /// Checks for substring in the string without case. 
        /// </summary>
        /// <param name="text">The string in which the search is made.</param>
        /// <param name="value">Search string.</param>
        /// <param name="stringComparison">Comparison type</param>
        /// <returns></returns>
        public static bool CaseInsensitiveContains(this string text, string value,
            StringComparison stringComparison = StringComparison.CurrentCultureIgnoreCase)
        {
            return text.IndexOf(value, stringComparison) >= 0;
        }
    }
}