﻿using System.Web;
using AutoMapper;
using Microsoft.Owin.Security;
using Ninject.Modules;
using Ninject.Web.Common;
using QuizMan.BLL.Infrastructure;
using QuizMan.BLL.Interfaces;
using QuizMan.BLL.Services;

namespace QuizMan.WEB.Utils
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<IMapper>().ToMethod(context =>
            {
                var config = new MapperConfiguration(mc => { mc.AddProfile(new AutoMapperProfile()); });
                return config.CreateMapper();
            }).InSingletonScope();

            Bind<ICategoryService>().To<CategoryService>();
            Bind<ITestService>().To<TestService>();
            Bind<IUserService>().To<UserService>();
            Bind<ILoggerService>().To<LoggerService>();

            Bind<IAuthenticationManager>().ToMethod(
                c =>
                    HttpContext.Current.GetOwinContext().Authentication).InRequestScope();
        }
    }
}