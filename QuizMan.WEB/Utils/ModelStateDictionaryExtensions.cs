﻿using System.Collections.Generic;
using System.Web.Mvc;


namespace QuizMan.WEB.Utils
{
    /// <summary>
    /// Model state extension
    /// </summary>
    public static class ModelStateDictionaryExtensions
    {
        /// <summary>
        /// Merges errors
        /// </summary>
        /// <param name="modelState">Model state</param>
        /// <param name="dictionary">Errors dictionary</param>
        /// <param name="prefix">Prefix of key</param>
        public static void Merge(this ModelStateDictionary modelState, IDictionary<string, string> dictionary, string prefix)
        {
            foreach (var item in dictionary)
            {
                modelState.AddModelError((string.IsNullOrEmpty(prefix) ? "" : (prefix + ".")) + item.Key, item.Value);
            }
        }
    }
}