﻿namespace QuizMan.WEB.Utils
{
    /// <summary>
    /// Application roles
    /// </summary>
    public static class Role
    {
        public const string Admin = "admin";
        public const string Editor = "editor";
    }
}