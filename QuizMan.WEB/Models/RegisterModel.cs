﻿using System.ComponentModel.DataAnnotations;

namespace QuizMan.WEB.Models
{
    public class RegisterModel
    {
        [Required]
        [EmailAddress]
        [MaxLength(100)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MaxLength(200)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        [MaxLength(200)]
        public string ConfirmPassword { get; set; }

        [Required]
        [MaxLength(75)]
        public string Surname { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [Range(1,200)]
        public int Age { get; set; }
    }
}