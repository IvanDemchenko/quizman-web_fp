﻿using System.ComponentModel.DataAnnotations;

namespace QuizMan.WEB.Models
{
    public class LoginModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}