﻿using System.Collections.Generic;

namespace QuizMan.WEB.Models
{
    public class UserRolesModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Git { get; set; }
        public int Year { get; set; }
        public bool IsBanned { get; set; }
        public IEnumerable<string> Roles { get; set; }
    }
}