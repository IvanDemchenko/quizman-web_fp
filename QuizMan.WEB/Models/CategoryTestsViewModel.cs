﻿using System;
using System.Collections.Generic;
using QuizMan.BLL.DataTransferObjects;

namespace QuizMan.WEB.Models
{
    public class CategoryTestsViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; }
        public IEnumerable<TestModel> Tests { get; set; }
    }
}