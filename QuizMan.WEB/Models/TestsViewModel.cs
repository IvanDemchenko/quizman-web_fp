﻿using System.Collections.Generic;
using QuizMan.BLL.DataTransferObjects;

namespace QuizMan.WEB.Models
{
    public class TestsViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public IEnumerable<TestModel> Tests { get; set; }
    }
}