﻿function validateForm() {
    let valid = true;
    const optionsContainer = document.getElementsByClassName('options-container');
    for (let i = 0; i < optionsContainer.length; i++) {
        const inputs = [...optionsContainer[i].getElementsByTagName('input')].filter(x => x.type === "radio");
        if(inputs.length === 0) continue;
        if (!(inputs.some(x => x.checked === true))) {
            inputs.forEach(x => x.className += " radio-invalid");
            valid = false;
        }
    }
    console.log(valid);
    return valid;
}


function createCardBlock(questionNumber) {
    const questionContainer = document.createElement('div');
    questionContainer.innerHTML = document.getElementById('question-block').innerHTML;
    questionContainer.className = 'question-container mb-3';
    questionContainer.dataset.questionNumber = questionNumber;
    questionContainer.dataset.inputType = "radio";
    const optionsBlock = questionContainer.querySelector('.options-container');
    const pat = /Questions\[[0-9]+\]/;
    const inputs = questionContainer.querySelectorAll('input,textarea,select');
    for (let j = 0; j < inputs.length; j++)
        inputs[j].name = inputs[j].name.replace(pat, `Questions[${questionNumber}]`);
    optionsBlock.appendChild(createRadioCheckInput('radio', questionNumber, 0));
    return questionContainer;
}

function createRadioCheckInput(inputType, questionNumber, optionNumber) {
    const inputContainer = document.createElement('div');
    inputContainer.innerHTML = document.getElementById('radio-check-input').innerHTML;
    inputContainer.className = 'input-container';
    const inputs = inputContainer.getElementsByTagName('input');
    inputs[0].type = inputType;
    for (let j = 0; j < inputs.length; j++) {
        const pat = /Questions\[[0-9]+\].Options\[[0-9]+\]/;
        inputs[j].name = inputs[j].name.replace(pat, `Questions[${questionNumber}].Options[${optionNumber}]`);
    }
    return inputContainer;
}

function createTextAreaInput(questionNumber) {
    const inputContainer = document.createElement('div');
    inputContainer.innerHTML = document.getElementById('text-answer-option').innerHTML;
    inputContainer.className = 'input-container';
    const inputs = inputContainer.getElementsByTagName('input');
    const pat = /Questions\[[0-9]+\]/;
    inputs[0].name = inputs[0].name.replace(pat, `Questions[${questionNumber}]`);
    return inputContainer;
}

function add_option(element) {
    const question_container = element.closest('.question-container');
    const input_type = question_container.dataset.inputType;
    const question_number = question_container.dataset.questionNumber;
    const options_container = question_container.querySelector('.options-container');
    add_option_field(options_container, input_type, question_number);
}

function add_option_field(nodeHandler, inputType, questionNumber) {
    if (inputType != "text") {
        const optionNumber = nodeHandler.getElementsByClassName('input-container').length;
        nodeHandler.appendChild(createRadioCheckInput(inputType, questionNumber, optionNumber));
    } else {
        nodeHandler.appendChild(createTextAreaInput(questionNumber));
    }
}

function deleteOption(element) {
    const input_container = element.closest('.input-container');
    const options_container = input_container.closest('.options-container');
    const inputGroupsCount = options_container.getElementsByClassName('input-container').length;

    if (inputGroupsCount == 1)
        return;
    input_container.parentNode.removeChild(input_container);
    const pat = /Options\[[0-9]+\]/;
    const inputGroups = options_container.getElementsByClassName('input-container');
    for (let i = 0; i < inputGroups.length; i++) {
        const inputs = inputGroups[i].getElementsByTagName('input');
        for (let j = 0; j < inputs.length; j++)
            inputs[j].name = inputs[j].name.replace(pat, `Options[${i}]`);
    }
}

function deleteQuestion(element) {
    const pat = /Questions\[[0-9]+\]/;
    const question_container = element.closest('.question-container');
    question_container.parentNode.removeChild(question_container);
    const testBlock = document.getElementsByClassName('test-block')[0];
    const question_containers = testBlock.getElementsByClassName('question-container');
    for (let i = 0; i < question_containers.length; i++) {
        const inputs = question_containers[i].querySelectorAll('input,textarea,select');
        question_containers[i].dataset.questionNumber = i;
        for (let j = 0; j < inputs.length; j++)
            inputs[j].name = inputs[j].name.replace(pat, `Questions[${i}]`);
    }
}

function selectTypeManager(element) {
    const selectValue = element.value;
    const question_container = element.closest('.question-container');
    const previousInputType = question_container.dataset.inputType;
    question_container.dataset.inputType = selectValue;
    const questionNumber = question_container.dataset.questionNumber;
    const optBlock = question_container.querySelector(".options-container");

    if (selectValue !== "text") {
        if (previousInputType == "checkbox" || previousInputType == "radio") {
            const inputs = optBlock.getElementsByTagName('input');
            for (let i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "radio" || inputs[i].type == "checkbox") {
                    inputs[i].type = selectValue;
                    inputs[i].checked = false;
                }
            }
        } else {
            optBlock.innerHTML = "";
            add_option_field(optBlock, selectValue, questionNumber);
            question_container.querySelector('.card').insertAdjacentHTML('beforeend', document.getElementById('add-option-button').innerHTML);
        }
    } else {
        optBlock.innerHTML = "";
        add_option_field(optBlock, selectValue, questionNumber);
        const card_footer = question_container.querySelector(".card-footer");
        if (card_footer != null)
            card_footer.parentNode.removeChild(card_footer);
    }
}

function radioManager(element) {
    const inputsBlock = element.closest(".options-container").getElementsByTagName("input");
    for (let i = 0; i < inputsBlock.length; i++)
        if (inputsBlock[i].type == "radio")
            if (inputsBlock[i] !== element) {
                inputsBlock[i].checked = false;
                inputsBlock[i].className = "form-check-input is-valid";
            }
             
}

function addQuestion(element) {
    const testBlock = document.getElementsByClassName('test-block')[0];
    const numberOfQuestions = testBlock.getElementsByClassName('question-container').length;
    testBlock.appendChild(createCardBlock(numberOfQuestions));
}