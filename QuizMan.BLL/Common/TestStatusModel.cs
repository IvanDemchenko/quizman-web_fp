﻿using QuizMan.DAL.Entities;

namespace QuizMan.BLL.Common
{
    /// <summary>
    /// An enumeration of test states.
    /// </summary>
    public enum TestState : byte
    {
        NotStarted,
        InProgress,
        Finished,
        Expired,
        Success,
        Failure
    }

    /// <summary>
    /// Test status model
    /// </summary>
    public class TestStatusModel
    {
        /// <summary>
        /// Test State
        /// </summary>
        public TestState State { get; set; }

        /// <summary>
        /// Test status
        /// </summary>
        public TestStatus TestStatus { get; set; }
    }
}