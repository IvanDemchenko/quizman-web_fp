﻿using Ninject.Modules;
using QuizMan.DAL.Interfaces;
using QuizMan.DAL.Repositories;

namespace QuizMan.BLL.Infrastructure
{
    /// <summary>
    /// Dependency Injection module.
    /// </summary>
    public class ServiceModule:NinjectModule
    {
        private readonly string _connectionString;

        /// <summary>
        /// Initializes an instance of dependency injection module.
        /// </summary>
        /// <param name="connection">Database connection string</param>
        public ServiceModule(string connection) => _connectionString = connection;

        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().WithConstructorArgument(_connectionString);
        }
    }
}
