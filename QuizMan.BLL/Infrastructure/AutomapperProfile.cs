﻿using AutoMapper;
using QuizMan.BLL.DataTransferObjects;
using QuizMan.DAL.Entities;
using QuizMan.DAL.Entities.Identity;
using QuizMan.DAL.Entities.Log;

namespace QuizMan.BLL.Infrastructure
{
    /// <summary>
    /// Represents configurations of mapping
    /// </summary>
    public class AutoMapperProfile:Profile
    {
        /// <summary>
        /// Initializes an instance of AutoMapper profile with map configurations 
        /// </summary>
        public AutoMapperProfile()
        {
            CreateMap<Category, CategoryModel>().ReverseMap();
            CreateMap<Test, TestModel>().ReverseMap()
                .ForPath(x => x.CreationDate, x => x.Ignore());
            CreateMap<Question, QuestionModel>().ReverseMap();
            CreateMap<Option, OptionModel>().ForMember(x=>x.IsAnswer, o=>
                o.MapFrom(x=>x.IsAnswer ? "on" : null))
                .ReverseMap()
                .ForPath(o=>o.IsAnswer, opt=>
                    opt.MapFrom(x=>x.IsAnswer == "on"));
            CreateMap<ApplicationUser, ApplicationUserModel>().IncludeMembers(x=>x.ClientProfile).ReverseMap();
            CreateMap<ClientProfile, ApplicationUserModel>().ReverseMap();
            CreateMap<QuickResult, QuickResultModel>()
                .ForMember(x => x.Duration, y => y.MapFrom(z => z.TestStatus.FinishedAt.Value.Subtract(z.TestStatus.StartedAt)))
                .ForMember(x => x.Title, y => y.MapFrom(z => z.TestStatus.Test.Title))
                .ForMember(x => x.TestId, y => y.MapFrom(z => z.TestStatus.TestId))
                .ReverseMap();
            CreateMap<ExceptionDetail, ExceptionDetailModel>().ReverseMap();
        }
    }
}
