﻿using System;
using System.Runtime.Serialization;

namespace QuizMan.BLL.Infrastructure
{
    /// <summary>
    /// Exception of incorrect test operation.
    /// </summary>
    [Serializable]
    public class TestException:Exception
    {
        /// <summary>
        /// Initializes an instance of exception
        /// </summary>
        /// <param name="message">Exception message</param>
        public TestException(string message):base(message) { }
        public TestException(string message, Exception innerException) : base(message, innerException) { }
        protected TestException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
