﻿using System;
using System.Runtime.Serialization;

namespace QuizMan.BLL.Infrastructure
{
    /// <summary>
    /// Exception of validation.
    /// </summary>
    [Serializable]
    public class ValidationException:Exception
    {
        /// <summary>
        /// Message for user
        /// </summary>
        public string UserMessage { get; set; }

        /// <summary>
        /// Initializes an instance of exception
        /// </summary>
        /// <param name="message">Exception message</param>
        public ValidationException(string message) : base(message) { }

        /// <summary>
        /// Initializes an instance of exception
        /// </summary>
        /// <param name="message">Message for admins and developers</param>
        /// <param name="userMessage">Message for users</param>
        public ValidationException(string message, string userMessage) : base(message)
        {
            UserMessage = userMessage;
        }
        public ValidationException(string message, Exception innerException) : base(message, innerException) { }
        protected ValidationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
