﻿namespace QuizMan.BLL.Infrastructure
{
    /// <summary>
    /// Model of operation result
    /// </summary>
    public class OperationDetails
    {
        /// <summary>
        /// Initializes an instance of operation result
        /// </summary>
        /// <param name="succeeded">Status of execution</param>
        /// <param name="message">Operation result message</param>
        /// <param name="prop">Property</param>
        public OperationDetails(bool succeeded, string message, string prop)
        {
            Succeeded = succeeded;
            Message = message;
            Property = prop;
        }

        public bool Succeeded { get; }
        public string Message { get; }
        public string Property { get; }
    }
}
