﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using QuizMan.BLL.DataTransferObjects;
using QuizMan.BLL.Infrastructure;
using QuizMan.BLL.Interfaces;
using QuizMan.DAL.Entities;
using QuizMan.DAL.Interfaces;

namespace QuizMan.BLL.Services
{
    /// <summary>
    /// Service for working with categories
    /// </summary>
    public class CategoryService:ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes an instance of category service
        /// </summary>
        /// <param name="unitOfWork">Unit of Work</param>
        /// <param name="mapper">AutoMapper</param>
        public CategoryService(IUnitOfWork unitOfWork, IMapper mapper )
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        /// <summary>
        /// Method of model validation.
        /// </summary>
        /// <param name="model">Category model</param>
        /// <returns></returns>
        public IDictionary<string, string> ValidateCategory(CategoryModel model)
        {
            var errors = new Dictionary<string, string>();
            if(string.IsNullOrEmpty(model.Title))
                errors.Add("Title", "The title is required field");
            else if(model.Title.Length > 75)
                errors.Add("Title", "The title should be no more than 75 characters.");
            if (string.IsNullOrEmpty(model.Description))
                errors.Add("Description", "The description is required field");
            else if(model.Description.Length > 1000)
                errors.Add("Description", "The description should be no more than 1000 characters.");
            return errors;
        }

        /// <summary>
        /// Returns all categories with short description 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CategoryModel> GetAllCategoriesWithShortDescription()
        {
            var categories = _unitOfWork.CategoryRepository.GetAll().ToList();
            var categoriesModel = _mapper.Map<IEnumerable<CategoryModel>>(categories).ToList();
            foreach (var category in categoriesModel.Where(category => category.Description.Length > 100))
                category.Description = category.Description.Substring(0, 100) + "...";
            return categoriesModel;
        }

        /// <summary>
        /// Returns the category by id
        /// </summary>
        /// <param name="id">Category primary key</param>
        /// <returns></returns>
        public async Task<CategoryModel> GetCategoryByIdAsync(int id)
        {
            var category = await _unitOfWork.CategoryRepository.GetByIdAsync(id);
            if(category is null)
                throw new ValidationException("", "There is no such category.");
            return _mapper.Map<CategoryModel>(category);
        }

        /// <summary>
        /// Returns the category by id with tests
        /// </summary>
        /// <param name="id">Category primary key</param>
        /// <returns></returns>
        public async Task<CategoryModel> GetCategoryWithDetailsByIdAsync(int id)
        {
            var category = (await _unitOfWork.CategoryRepository.GetWithIncludeAsync(x=>x.Id == id,includeProperties:x=>x.Tests)).FirstOrDefault();
            if (category is null)
                throw new ValidationException("", "There is no such category.");
            return _mapper.Map<CategoryModel>(category);
        }

        /// <summary>
        /// Adds or Updates Category
        /// </summary>
        /// <param name="categoryModel">Category model</param>
        /// <returns></returns>
        public async Task<int> AddOrUpdateCategory(CategoryModel categoryModel)
        {
            Category category;
            if (categoryModel.Id == 0)
            {
                category = new Category
                {
                    Title = categoryModel.Title,
                    Description = categoryModel.Description,
                    CreationDate = DateTime.Now
                };
                _unitOfWork.CategoryRepository.Add(category);
                await _unitOfWork.SaveAsync();
                return category.Id;
            }
            category = await _unitOfWork.CategoryRepository.GetByIdAsync(categoryModel.Id);
            if(category is null)
                throw new ValidationException("", "There is no such category.");
            category.Title = categoryModel.Title;
            category.Description = categoryModel.Description;
            _unitOfWork.CategoryRepository.Update(category);
            await _unitOfWork.SaveAsync();
            return 0;
        }

        /// <summary>
        /// Deletes the category by category id
        /// </summary>
        /// <param name="categoryId">Category primary key</param>
        /// <returns></returns>
        public async Task DeleteCategory(int categoryId)
        {
            var categoryDb = (await _unitOfWork.CategoryRepository
                    .GetWithIncludeAsync(
                    x => x.Id == categoryId,
                         includeProperties: x => x.Tests))
                    .FirstOrDefault();

            if(categoryDb is null)
                throw new ValidationException("", "Category does not exist");
            if (categoryDb.Title == "Other")
                throw new ValidationException("","You can not delete Other category");

            var otherCategory = _unitOfWork.CategoryRepository.Get(x => x.Title == "Other").FirstOrDefault();
            foreach (var test in categoryDb.Tests)
                test.Category = otherCategory;
            await _unitOfWork.CategoryRepository.DeleteByIdAsync(categoryDb.Id);
            await _unitOfWork.SaveAsync();
        }
        
        /// <summary>
        /// Returns number of category
        /// </summary>
        /// <returns></returns>
        public int CategoriesNumber()
        {
            return _unitOfWork.CategoryRepository.Count();
        }
    }
}
