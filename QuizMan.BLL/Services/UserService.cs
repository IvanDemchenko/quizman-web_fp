﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNet.Identity;
using QuizMan.BLL.DataTransferObjects;
using QuizMan.BLL.DataTransferObjects.UserManage;
using QuizMan.BLL.Infrastructure;
using QuizMan.BLL.Interfaces;
using QuizMan.DAL.Entities.Identity;
using QuizMan.DAL.Interfaces;

namespace QuizMan.BLL.Services
{
    /// <summary>
    /// Service for working with categories
    /// </summary>
    public class UserService: IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes an instance of test service
        /// </summary>
        /// <param name="unitOfWork">Unit of Work</param>
        /// <param name="mapper">AutoMapper</param>
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        /// <summary>
        /// Creates new user
        /// </summary>
        /// <param name="userModel">User model</param>
        /// <returns></returns>
        public async Task<OperationDetails> Create(UserModel userModel)
        {
            var user = await _unitOfWork.UserManager.FindByEmailAsync(userModel.Email);
            if (!(user is null)) return new OperationDetails(false, "A user with this Email already exists.", "Email");

            user = new ApplicationUser {Email = userModel.Email, UserName = userModel.Email, LockoutEnabled = true};
            var result = await _unitOfWork.UserManager.CreateAsync(user, userModel.Password);
            if(result.Errors.Any())
                return new OperationDetails(false, result.Errors.FirstOrDefault(),"");
            await _unitOfWork.UserManager.AddToRoleAsync(user.Id, userModel.Role);
            var clientProfile = new ClientProfile { Id = user.Id, Surname = userModel.Surname,FirstName = userModel.Name, Age = userModel.Age};
            _unitOfWork.ClientManager.Create(clientProfile);
            await _unitOfWork.SaveAsync();
            return new OperationDetails(true, "Registration completed successfully","");
        }

        /// <summary>
        /// Authenticates user
        /// </summary>
        /// <param name="userModel">User model</param>
        /// <returns></returns>
        public async Task<ClaimsIdentity> Authenticate(UserModel userModel)
        {
            ClaimsIdentity claims = null;
            var user = await _unitOfWork.UserManager.FindAsync(userModel.Email, userModel.Password);
            if (!(user is null))
                claims = await _unitOfWork.UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            return claims;
        }

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="id">User primary key</param>
        /// <returns></returns>
        public async Task<ApplicationUserModel> GetByIdAsync(string id)
        {
            var user = await _unitOfWork.UserManager.FindByIdAsync(id);
            if(user is null)
                throw new ValidationException("", "User is not found.");
            return  _mapper.Map<ApplicationUserModel>(user);
        }

        /// <summary>
        /// Returns all users
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ApplicationUserModel> GetAllUsers()
        {
            var users = _unitOfWork.UserManager.Users.Include(x=>x.Roles).ToList();
            return _mapper.Map<IEnumerable<ApplicationUserModel>>(users).ToList();
        }

        /// <summary>
        /// Returns user roles
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        public async Task<List<string>> GetUserRoles(string userId)
        {
            return new List<string>(await _unitOfWork.UserManager.GetRolesAsync(userId));
        }

        /// <summary>
        /// Returns all roles
        /// </summary>
        /// <returns></returns>
        public List<RoleModel> GetAllRoles()
        {
            var roles = _unitOfWork.RoleManager.Roles;
            var rolesModel = new List<RoleModel>();
            foreach (var role in roles)
            {
                rolesModel.Add(new RoleModel
                {
                    Id = role.Id,
                    Name = role.Name
                });
            }
            return rolesModel;
        }

        /// <summary>
        /// Checks if the user has a role.  
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <param name="name">Role name</param>
        /// <returns></returns>
        public async Task<bool> UserIsInRole(string userId, string name)
        {
            return await _unitOfWork.UserManager.IsInRoleAsync(userId, name);
        }

        /// <summary>
        /// Updates user roles
        /// </summary>
        /// <param name="editUserRolesModel">User roles model</param>
        /// <returns></returns>
        public async Task UpdateUserRoles(EditUserRolesModel editUserRolesModel)
        {
            var user = await _unitOfWork.UserManager.FindByIdAsync(editUserRolesModel.UserId);
            var roles = await _unitOfWork.UserManager.GetRolesAsync(user.Id);
            var result = await _unitOfWork.UserManager.RemoveFromRolesAsync(user.Id, roles.ToArray());
            if (!result.Succeeded)
            {
                throw new ValidationException("", "Roles were not removed.");
            }

            result = await _unitOfWork.UserManager.AddToRolesAsync(user.Id,
                editUserRolesModel.SelectedRoles.Where(x => x.Selected).Select(y => y.RoleName).ToArray());
            if (!result.Succeeded)
            {
                throw new ValidationException("", "Roles were not added.");
            }
        }

        /// <summary>
        /// Updates user profile
        /// </summary>
        /// <param name="applicationUserModel"></param>
        /// <returns></returns>
        public async Task UpdateUserProfile(ApplicationUserModel applicationUserModel)
        {
            var user =await _unitOfWork.UserManager.FindByIdAsync(applicationUserModel.Id);
            user.ClientProfile.FirstName = applicationUserModel.FirstName;
            user.ClientProfile.Surname = applicationUserModel.Surname;
            user.ClientProfile.Git = applicationUserModel.Git;
            await _unitOfWork.SaveAsync();
        }

        /// <summary>
        /// Changes user password
        /// </summary>
        /// <param name="changePasswordModel">Password model</param>
        /// <returns></returns>
        public async Task ChangePassword(ChangePasswordModel changePasswordModel)
        {
            await _unitOfWork.UserManager.ChangePasswordAsync(changePasswordModel.UserId, changePasswordModel.OldPassword, changePasswordModel.NewPassword);
        }

        /// <summary>
        /// Checks if the user is banned.
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        public async Task<bool> IsLockedOutAsync(string userId)
        {
            return await _unitOfWork.UserManager.IsLockedOutAsync(userId);
        }

        /// <summary>
        /// Returns lockout end date
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        public async Task<DateTimeOffset> GetLockoutEndDateAsync(string userId)
        {
            return await _unitOfWork.UserManager.GetLockoutEndDateAsync(userId);
        }

        /// <summary>
        /// Sets lockout date
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <param name="days">Number of days</param>
        /// <returns></returns>
        public async Task<OperationDetails> SetLockOutDate(string userId, int days)
        {
            var result = await _unitOfWork.UserManager.SetLockoutEndDateAsync(userId, DateTimeOffset.UtcNow.AddDays(days));
            if (result.Errors.Any())
                return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
            return new OperationDetails(true, $"User was successfully banned for {days} days", "");
        }

        /// <summary>
        /// Removes the ban from the user 
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        public async Task<OperationDetails> UnBan(string userId)
        {
            var result = await _unitOfWork.UserManager.SetLockoutEndDateAsync(userId, DateTimeOffset.MinValue);
            return result.Errors.Any() ?
                new OperationDetails(false, result.Errors.FirstOrDefault(), "") :
                new OperationDetails(true, $"User was successfully unbanned", "");
        }

        /// <summary>
        /// Returns number of users
        /// </summary>
        /// <returns></returns>
        public int UsersNumber()
        {
            return _unitOfWork.UserManager.Users.AsNoTracking().Select(x => new {x.Id}).Count();
        }
    }
}
