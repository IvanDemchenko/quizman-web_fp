﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using QuizMan.BLL.Common;
using QuizMan.BLL.DataTransferObjects;
using QuizMan.BLL.DataTransferObjects.AnswerModel;
using QuizMan.BLL.Infrastructure;
using QuizMan.BLL.Interfaces;
using QuizMan.DAL.Entities;
using QuizMan.DAL.Interfaces;

namespace QuizMan.BLL.Services
{
    /// <summary>
    /// Service for working with categories
    /// </summary>
    public class TestService:ITestService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes an instance of test service
        /// </summary>
        /// <param name="unitOfWork">Unit of Work</param>
        /// <param name="mapper">AutoMapper</param>
        public TestService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        /// <summary>
        /// Registers that the user has started the test.
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <param name="testId">Test primary key</param>
        /// <returns></returns>
        public async Task<DateTime> RegisterUserToTest(string userId, int testId)
        {
            var testStatusModel = await CheckTestStatus(userId, testId);
            switch (testStatusModel.State)
            {
                case TestState.Finished:
                    throw new TestException("Test has finished");
                case TestState.NotStarted:
                {
                    var user = await _unitOfWork.UserManager.FindByIdAsync(userId);
                    var test = await _unitOfWork.TestRepository.GetByIdAsync(testId);
                    if(user is null || test is null)
                        throw new ValidationException("", "No such user or test has been found. ");

                    var testStatus = new TestStatus
                    {
                        StartedAt = DateTime.Now,
                        Test = test,
                        ApplicationUser = user,
                    };

                    _unitOfWork.TestStatusRepository.Add(testStatus);
                    await _unitOfWork.SaveAsync();
                    return testStatus.StartedAt.AddMinutes(test.TimeToPass);
                }
                case TestState.InProgress:
                    return testStatusModel.TestStatus.StartedAt.AddMinutes(testStatusModel.TestStatus.Test.TimeToPass);
                default:
                    return DateTime.MinValue;
            }
        }

        /// <summary>
        /// Checks the test status by user. 
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <param name="testId">Test primary key</param>
        /// <returns></returns>
        public async Task<TestStatusModel> CheckTestStatus(string userId, int testId)
        {
            var startedTest = (await _unitOfWork.TestStatusRepository.GetWithIncludeAsync(
                    x => x.TestId == testId && x.ApplicationUserId == userId,
                    null,
                    x => x.Test.Questions.Select(y => y.Options),
                    x => x.Answers))
                .FirstOrDefault();

            if (startedTest is null)
                return new TestStatusModel { State = TestState.NotStarted };
            if (startedTest.FinishedAt != null)
                return new TestStatusModel { State = TestState.Finished, TestStatus = startedTest };
            if (startedTest.StartedAt.AddMinutes(startedTest.Test.TimeToPass) > DateTime.Now)
                return new TestStatusModel { State = TestState.InProgress, TestStatus = startedTest };
            return new TestStatusModel { State = TestState.Expired, TestStatus = startedTest };
        }

        /// <summary>
        /// Returns test with details
        /// </summary>
        /// <param name="id">Test primary key</param>
        /// <returns></returns>
        public async Task<TestModel> GetTestWithDetailsByIdAsync(int id)
        {
            var test = (await _unitOfWork.TestRepository.GetWithIncludeAsync(
                x => x.Id == id,
                includeProperties: x => x.Questions.Select(q => q.Options)))
                .FirstOrDefault();
            if(test is null)
                throw new ValidationException("", "No test found.");
            return _mapper.Map<TestModel>(test);
        }

        /// <summary>
        /// Adds user answer to database and calculates quick result
        /// </summary>
        /// <param name="testAnswersModel">Test answer model</param>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        public async Task AddUserAnswer(TestAnswersModel testAnswersModel, string userId)
        {
            var startedTest = (await _unitOfWork.TestStatusRepository.GetWithIncludeAsync(
                x => x.TestId == testAnswersModel.TestId && x.ApplicationUserId == userId,
                null,
                x => x.Test.Questions.Select(y => y.Options),
                x => x.Answers))
                .FirstOrDefault();

            if (startedTest != null && (startedTest.FinishedAt != null || startedTest.StartedAt.AddMinutes(startedTest.Test.TimeToPass + 1) < DateTime.Now))
                throw new TestException("This test already passed or time is down");
            if (startedTest is null)
                throw new TestException("Test not started");

            startedTest.FinishedAt = DateTime.Now;

            foreach (var answerModel in testAnswersModel.Answers)
            {
                if (answerModel.UserChooses is null)
                {
                    var answer = new Answer
                    {
                        QuestionId = answerModel.QuestionId,
                        TextAnswer = answerModel.Answer,
                        StartedTestId = startedTest.Id
                    };
                    _unitOfWork.AnswerRepository.Add(answer);
                }
                else
                {
                    foreach (var answer in answerModel.UserChooses
                        .Where(option => option.IsChecked == "on")
                        .Select(option => new Answer
                        {
                            QuestionId = answerModel.QuestionId,
                            OptionId = option.ChooseId,
                            StartedTestId = startedTest.Id
                        }))
                    {
                        _unitOfWork.AnswerRepository.Add(answer);
                    }
                }
            }
            CalculateQuickResult(startedTest);
            await _unitOfWork.SaveAsync();
        }

        /// <summary>
        /// Calculates quick result
        /// </summary>
        /// <param name="testStatus">Test status model</param>
        public void CalculateQuickResult(TestStatus testStatus)
        {
            var totalScore = 0;
            var pointsEarned = 0;

            foreach (var question in testStatus.Test.Questions)
            {
                var scoreBuff = question.Score;
                totalScore += question.Score;
                if (question.Type != "text")
                {
                    var answers = question.Options.Where(x => x.IsAnswer).Select(x => x.Id).ToArray();
                    var userAnswers = testStatus.Answers.Where(x => x.QuestionId == question.Id).Select(x => x.OptionId.Value).ToArray();
                    if (!(answers.Length == userAnswers.Length && !answers.Except(userAnswers).Any()))
                        scoreBuff = 0;
                }
                else
                {
                    var answer = question.Options.FirstOrDefault()?.Text;
                    var userTextAnswer = testStatus.Answers.FirstOrDefault(q => q.QuestionId == question.Id && q.TextAnswer != null)?.TextAnswer;
                    if (!string.Equals(userTextAnswer, answer, StringComparison.CurrentCultureIgnoreCase))
                        scoreBuff = 0;
                }
                pointsEarned += scoreBuff;
            }
            var testResult = new QuickResult { TotalPoints = totalScore, EarnedPoints = pointsEarned, Id = testStatus.Id };
            var necessaryPoints = (float)totalScore / 100 * testStatus.Test.TestPassingPercentage;
            testResult.IsPassed = pointsEarned >= necessaryPoints;
            _unitOfWork.QuickResultRepository.Add(testResult);
        }

        /// <summary>
        /// Returns the detailed result of the passed test. 
        /// </summary>
        /// <param name="testId">Test primary key</param>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        public async Task<DetailedTestResultModel> GetDetailedResult(int testId, string userId)
        {
            var startedTest = (await _unitOfWork.TestStatusRepository.GetWithIncludeAsync(
                x => x.TestId == testId && x.ApplicationUserId == userId,
                null,
                x => x.Test.Questions.Select(y => y.Options),
                x => x.Answers))
                .FirstOrDefault();

            if (startedTest is null)
                throw new TestException("Test not started");
            if (startedTest.FinishedAt is null)
                throw new TestException("The test is not yet complete or has expired.");

            var user = await _unitOfWork.UserManager.FindByIdAsync(userId);

            var detailedTestResult = new DetailedTestResultModel
            {
                TestTitle = startedTest.Test.Title,
                UserName = user.UserName,
                StartedAt = startedTest.StartedAt,
                FinishedAt = startedTest.FinishedAt,
                QuestionsResultModels = new List<QuestionResultModel>()
            };

            var totalScore = 0;
            var pointsEarned = 0;

            foreach (var question in startedTest.Test.Questions)
            {
                totalScore += question.Score;

                var questionResultModel = new QuestionResultModel
                {
                    Text = question.Text,
                    Type = question.Type,
                    OptionResultModels = new List<OptionResultModel>(),
                    IsCorrect = true,
                    Score = question.Score
                };

                if (question.Type != "text")
                {
                    foreach (var option in question.Options)
                    {
                        var optionResultModel = new OptionResultModel { Text = option.Text, IsAnswer = option.IsAnswer };

                        if (startedTest.Answers.Any(x => x.QuestionId == question.Id && x.OptionId == option.Id))
                            optionResultModel.UserAnswer = true;
                        if (optionResultModel.UserAnswer && !option.IsAnswer || option.IsAnswer && !optionResultModel.UserAnswer)
                        {
                            questionResultModel.IsCorrect = false;
                            questionResultModel.Score = 0;
                        }
                        questionResultModel.OptionResultModels.Add(optionResultModel);
                    }
                }
                else
                {
                    var optionResultModel = new OptionResultModel { Text = question.Options.FirstOrDefault()?.Text, IsAnswer = true };
                    questionResultModel.UserTextAnswer = startedTest.Answers.FirstOrDefault(q => q.QuestionId == question.Id && q.TextAnswer != null)?.TextAnswer;
                    questionResultModel.OptionResultModels.Add(optionResultModel);
                    if (!string.Equals(questionResultModel.UserTextAnswer,
                        questionResultModel.OptionResultModels.FirstOrDefault()?.Text, StringComparison.CurrentCultureIgnoreCase))
                    {
                        questionResultModel.IsCorrect = false;
                        questionResultModel.Score = 0;
                    }
                }
                pointsEarned += questionResultModel.Score;
                detailedTestResult.QuestionsResultModels.Add(questionResultModel);
            }

            detailedTestResult.PointsEarned = pointsEarned;
            detailedTestResult.TotalScore = totalScore;
            var necessaryPoints = (float)totalScore / 100 * startedTest.Test.TestPassingPercentage;
            detailedTestResult.IsComplete = pointsEarned >= necessaryPoints;
            return detailedTestResult;
        }

        /// <summary>
        /// Returns test information by id
        /// </summary>
        /// <param name="id">Test primary key</param>
        /// <returns></returns>
        public async Task<TestModel> GetTestById(int id)
        {
            var test = await _unitOfWork.TestRepository.GetByIdAsync(id);
            if(test is null)
                throw new ValidationException("", "No test found");
            return _mapper.Map<TestModel>(test);
        }

        /// <summary>
        /// Returns test with user progress by id
        /// </summary>
        /// <param name="id">Test primary key</param>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        public async Task<TestModel> GetTestByIdWithProgress(int id, string userId)
        {
            var testStatus = _unitOfWork.TestStatusRepository.GetWithInclude(
                x => x.ApplicationUserId == userId && x.TestId == id,
                includeProperties: x => x.QuickResult).FirstOrDefault();

            var test = await _unitOfWork.TestRepository.GetByIdAsync(id);
            if (test is null)
                throw new ValidationException("", "No test found");

            var testModel = _mapper.Map<TestModel>(test);

            if (testStatus == null) return testModel;

            if (testStatus.QuickResult != null)
                testModel.TestState = testStatus.QuickResult.IsPassed ? TestState.Success : TestState.Failure;
            else if (testStatus.StartedAt.AddMinutes(testStatus.Test.TimeToPass) > DateTime.Now)
                testModel.TestState = TestState.InProgress;
            return testModel;
        }

        /// <summary>
        /// Returns tests with user progress by user id and category id
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <param name="categoryId">Category primary key</param>
        /// <returns></returns>
        public IEnumerable<TestModel> GetTestsWithProgress(string userId, int? categoryId = null)
        {
            var testStatuses = _unitOfWork.TestStatusRepository.GetWithInclude(
                x => x.ApplicationUserId == userId,
                includeProperties: x => x.QuickResult);

            var tests = categoryId is null ?
                _unitOfWork.TestRepository.GetAll().AsEnumerable() :
                _unitOfWork.TestRepository.Get(x => x.CategoryId == categoryId.Value);

            var testModels = _mapper.Map<IEnumerable<TestModel>>(tests);
            var testsWithProgress = testModels.ToList();

            foreach (var testStatus in testStatuses)
            {
                var test = testsWithProgress.SingleOrDefault(x => x.Id == testStatus.TestId);
                if (test is null) continue;
                if (testStatus.QuickResult != null)
                    test.TestState = testStatus.QuickResult.IsPassed ? TestState.Success : TestState.Failure;
                else if (testStatus.StartedAt.AddMinutes(testStatus.Test.TimeToPass) > DateTime.Now)
                    test.TestState = TestState.InProgress;
            }
            return testsWithProgress;
        }

        /// <summary>
        /// Returns test quick result
        /// </summary>
        /// <param name="testId">Test primary key</param>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        public async Task<QuickResultModel> GetQuickResult(int testId, string userId)
        {
            var quickResult = (await _unitOfWork.QuickResultRepository.GetWithIncludeAsync(
                x => x.TestStatus.ApplicationUserId == userId && x.TestStatus.TestId == testId,
                null,
                x => x.TestStatus.Test))
                .FirstOrDefault();
            if (quickResult is null)
                throw new TestException("Test not passed yet");
            return _mapper.Map<QuickResultModel>(quickResult);
        }

        /// <summary>
        /// Returns tests quick results by user
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        public IEnumerable<QuickResultModel> GetTestQuickResultsByUserId(string userId)
        {
            var quickResults =  _unitOfWork.QuickResultRepository.GetWithInclude(
                x => x.TestStatus.ApplicationUserId == userId,
                null,
                x => x.TestStatus.Test);
            return _mapper.Map<IEnumerable<QuickResultModel>>(quickResults);
        }

        /// <summary>
        /// Returns asynchronously tests quick results by user
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        public async Task<IEnumerable<QuickResultModel>> GetTestQuickResultsByUserIdAsync(string userId)
        {
            var quickResults = await _unitOfWork.QuickResultRepository.GetWithIncludeAsync(
                x => x.TestStatus.ApplicationUserId == userId,
                null,
                x => x.TestStatus.Test);
            return _mapper.Map<IEnumerable<QuickResultModel>>(quickResults);
        }

        /// <summary>
        /// Returns tests from database
        /// </summary>
        /// <param name="categoryId">Category primary key</param>
        /// <returns></returns>
        public IEnumerable<TestModel> GetTests(int? categoryId = null)
        {
            var tests = categoryId is null ?
                _unitOfWork.TestRepository.GetAll().AsEnumerable() :
                _unitOfWork.TestRepository.Get(x => x.CategoryId == categoryId.Value);
            return _mapper.Map<IEnumerable<TestModel>>(tests);
        }

        /// <summary>
        /// Returns tests by category id
        /// </summary>
        /// <param name="id">Category primary key</param>
        /// <returns></returns>
        public IEnumerable<TestModel> GetTestsByCategoryId(int id)
        {
            var tests = _unitOfWork.TestRepository.Get(x => x.CategoryId == id);
            return _mapper.Map<IEnumerable<TestModel>>(tests);
        }

        /// <summary>
        /// Changes test status (active/ not active)
        /// </summary>
        /// <param name="testId">Test primary key</param>
        /// <returns></returns>
        public async Task ChangeTestStatus(int testId)
        {
            var test = await _unitOfWork.TestRepository.GetByIdAsync(testId);
            if (test is null)
                throw new ValidationException("", "No test found");
            if(test.Deprecated)
                throw new TestException("The status of the deleted test cannot be changed.");
            test.IsActive = !test.IsActive;
            await _unitOfWork.SaveAsync();
        }

        /// <summary>
        /// Returns number of tests
        /// </summary>
        /// <returns></returns>
        public int TestsNumber()
        {
            return _unitOfWork.TestRepository.Count();
        }

        /// <summary>
        /// Method of model validation.
        /// </summary>
        /// <param name="model">Test model</param>
        /// <returns></returns>
        public IDictionary<string, string> ValidateTestModel(TestModel model)
        {
            var errors = new Dictionary<string, string>();
            if (string.IsNullOrEmpty(model.Title))
                errors.Add(nameof(model.Title), "The title is required field");
            else if (model.Title.Length > 75)
                errors.Add(nameof(model.Title), "The title should be no more than 75 characters.");

            if (string.IsNullOrEmpty(model.Description))
                errors.Add(nameof(model.Description), "The description is required field");
            else if (model.Description.Length > 1000)
                errors.Add(nameof(model.Description), "The description should be no more than 1000 characters.");

            if (model.TimeToPass <= 0 || model.TimeToPass > 500)
                errors.Add(nameof(model.TimeToPass), "Time must be in range from 1 to 500");

            if (model.TestPassingPercentage <= 0 || model.TestPassingPercentage > 100)
                errors.Add(nameof(model.TestPassingPercentage), "Test Passing Percentage must be in range from 1 to 500");

            if (model.Questions is null)
            {
                errors.Add("Questions", "The test must have at least one question ");
                return errors;
            }

            var i = 0;
            foreach (var question in model.Questions)
            {
                if (question.Score <= 0 || question.Score >= 200)
                    errors.Add($"Questions[{i}].Score", "Score must be in range from 1 to 200");

                if (string.IsNullOrEmpty(question.Text))
                    errors.Add($"Questions[{i}].Text", "The question text is required field");

                else if (question.Text.Length > 500)
                    errors.Add($"Questions[{i}].Text", "The question should be no more than 500 characters.");

                if (question.Type != "radio" && question.Type != "text" && question.Type != "checkbox")
                    errors.Add($"Questions[{i}].Type", "The question type should be text, radio or checkbox");

                var j = 0;
                foreach (var option in question.Options)
                {
                    if(string.IsNullOrEmpty(option.Text))
                        errors.Add($"Questions[{i}].Options[{j}].Text", "The option text is required field");
                    else if(option.Text.Length > 500)
                        errors.Add($"Questions[{i}].Options[{j}].Text", "The description should be no more than 50 characters.");
                    j++;
                }
                if (question.Type != null && question.Type == "radio" && question.Options.All(x => x.IsAnswer != "on"))
                    errors.Add($"Questions[{i}].Options", "The single question options must have answer");
                i++;
            }
            return errors;
        }
       
        /// <summary>
        /// Deletes test by id
        /// </summary>
        /// <param name="id">Test primary key</param>
        /// <returns></returns>
        public async Task DeleteTestById(int id)
        {
            var testDb = (await _unitOfWork.TestRepository.GetWithIncludeAsync(x => x.Id == id, includeProperties: x => x.Questions.Select(q => q.Options))).SingleOrDefault();
            
            if(testDb is null)
                throw new ValidationException("", "No such test was found.");
            if (testDb.Deprecated)
                throw new TestException("The test is already considered deleted.");
            var startedTest = _unitOfWork.TestStatusRepository.Get(x => x.TestId == id);
            if (!startedTest.Any())
                await _unitOfWork.TestRepository.DeleteByIdAsync(id);
            else
            {
                testDb.IsActive = false;
                testDb.Deprecated = true;
                _unitOfWork.TestRepository.Update(testDb);
            }
            await _unitOfWork.SaveAsync();
        }

        /// <summary>
        /// Adds or updates test
        /// </summary>
        /// <param name="testModel">Test model</param>
        /// <returns></returns>
        public async Task<int> AddOrUpdateTest(TestModel testModel)
        {
            var id = testModel.Id;
            if (id == 0)
            {
                var test = _mapper.Map<Test>(testModel);
                test.CreationDate = DateTime.Now;
                _unitOfWork.TestRepository.Add(test);
            }
            else
            {
                var testDb = (await _unitOfWork.TestRepository.GetWithIncludeAsync(x => x.Id == testModel.Id, includeProperties: x => x.Questions.Select(q => q.Options))).SingleOrDefault();
                if(testDb is null)
                    throw new ValidationException("", "There is no such test.");
                var startedTest = _unitOfWork.TestStatusRepository.Get(x => x.TestId == id);
                if (!startedTest.Any())
                {
                    var testMap = _mapper.Map(testModel, testDb);
                    _unitOfWork.TestRepository.UpdateTestWithDetails(testMap);
                }
                else
                {
                    testDb.Deprecated = true;
                    _unitOfWork.TestRepository.Update(testDb);
                    var test = _mapper.Map<Test>(testModel);
                    test.CreationDate = DateTime.Now;
                    _unitOfWork.TestRepository.Add(test);
                }
            }
            await _unitOfWork.SaveAsync();
            return id;
        }

        /// <summary>
        /// Returns the top 3 users by the number of passed tests. 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TopUserModel> GetTopUsers()
        {
            var result = _unitOfWork.QuickResultRepository
                .GetWithInclude(x => x.IsPassed, null, x => x.TestStatus.ApplicationUser.ClientProfile)
                .GroupBy(x => x.TestStatus.ApplicationUser).OrderByDescending(x => x.Count()).Select(x =>
                    new TopUserModel
                    {
                        Id = x.Key.Id,
                        Name = x.Key.ClientProfile.FirstName,
                        Surname = x.Key.ClientProfile.Surname,
                        TestPassed = x.Count()
                    }).Take(3);
            return result;
        }
    }
}
