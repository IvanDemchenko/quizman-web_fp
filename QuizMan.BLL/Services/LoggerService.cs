﻿using System.Collections.Generic;
using AutoMapper;
using QuizMan.BLL.DataTransferObjects;
using QuizMan.BLL.Interfaces;
using QuizMan.DAL.Entities.Log;
using QuizMan.DAL.Interfaces;

namespace QuizMan.BLL.Services
{
    /// <summary>
    /// Exceptions logging service.
    /// </summary>
    public class LoggerService: ILoggerService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes an instance of logger service
        /// </summary>
        /// <param name="unitOfWork">Unit of Work</param>
        /// <param name="mapper">AutoMapper</param>
        public LoggerService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        /// <summary>
        /// Writes log to database
        /// </summary>
        /// <param name="exceptionDetailModel">Exception detail model</param>
        public void WriteLog(ExceptionDetailModel exceptionDetailModel)
        {
            _unitOfWork.ExceptionDetailRepository.Add(_mapper.Map<ExceptionDetail>(exceptionDetailModel));
            _unitOfWork.Save();
        }

        /// <summary>
        /// Returns all logs
        /// </summary>
        /// <returns></returns>
        public  IEnumerable<ExceptionDetailModel> GetAllLogs()
        {
            return _mapper.Map<IEnumerable<ExceptionDetailModel>>(_unitOfWork.ExceptionDetailRepository.GetAll());
        }
    }
}
