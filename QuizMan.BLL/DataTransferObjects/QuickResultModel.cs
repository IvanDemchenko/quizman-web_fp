﻿using System;

namespace QuizMan.BLL.DataTransferObjects
{
    public class QuickResultModel
    {
        public string Title { get; set; }
        public string TestId { get; set; }
        public int TotalPoints { get; set; }
        public int EarnedPoints { get; set; }
        public bool IsPassed { get; set; }
        public TimeSpan Duration { get; set; }
    }
}
