﻿using System;

namespace QuizMan.BLL.DataTransferObjects
{
    public class ExceptionDetailModel
    {
        public int Id { get; set; }
        public string ExceptionMessage { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string StackTrace { get; set; }
        public DateTime Date { get; set; }
    }
}
