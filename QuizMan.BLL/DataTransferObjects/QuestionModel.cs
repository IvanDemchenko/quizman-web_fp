﻿using System.Collections.Generic;

namespace QuizMan.BLL.DataTransferObjects
{
    public class QuestionModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
        public int Score { get; set; }
        public virtual List<OptionModel> Options { get; set; }
    }
}
