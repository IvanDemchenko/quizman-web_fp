﻿using System;
using System.Collections.Generic;
using QuizMan.BLL.DataTransferObjects.AnswerModel;

namespace QuizMan.BLL.DataTransferObjects
{
    public class DetailedTestResultModel
    {
        public string UserName { get; set; }
        public DateTime StartedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
        public int TotalScore { get; set; }
        public int PointsEarned { get; set; }
        public bool IsComplete { get; set; }
        public string TestTitle { get; set; }
        public List<QuestionResultModel> QuestionsResultModels { get; set; }
    }
}
