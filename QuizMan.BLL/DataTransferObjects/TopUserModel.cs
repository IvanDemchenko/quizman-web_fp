﻿namespace QuizMan.BLL.DataTransferObjects
{
    public class TopUserModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int TestPassed { get; set; }
    }
}