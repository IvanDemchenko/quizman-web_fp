﻿using System.Collections.Generic;

namespace QuizMan.BLL.DataTransferObjects.UserManage
{
    public class EditUserRolesModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string UserSurname { get; set; }
        public List<SelectedRolesModel> SelectedRoles { get; set; }
    }
}