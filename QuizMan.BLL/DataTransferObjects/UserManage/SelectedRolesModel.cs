﻿namespace QuizMan.BLL.DataTransferObjects.UserManage
{
    public class SelectedRolesModel
    {
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public bool Selected { get; set; }
    }
}