﻿using System.Collections.Generic;

namespace QuizMan.BLL.DataTransferObjects.AnswerModel
{
    public class QuestionResultModel
    {
        public string Text { get; set; }
        public string Type { get; set; }
        public int Score { get; set; }
        public bool IsCorrect { get; set; }
        public List<OptionResultModel> OptionResultModels { get; set; }
        public string UserTextAnswer { get; set; }
    }
}
