﻿using System.Collections.Generic;

namespace QuizMan.BLL.DataTransferObjects.AnswerModel
{
    public class AnswerModel
    {
        public int QuestionId { get; set; }
        public List<ChooseModel> UserChooses { get; set; }
        public string Answer { get; set; }
    }
}