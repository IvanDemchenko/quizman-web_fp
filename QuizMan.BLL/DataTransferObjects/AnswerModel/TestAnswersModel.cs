﻿using System.Collections.Generic;

namespace QuizMan.BLL.DataTransferObjects.AnswerModel
{
    public class TestAnswersModel
    {
        public int TestId { get; set; }
        public List<AnswerModel> Answers { get; set; }
    }
}