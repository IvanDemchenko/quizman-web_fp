﻿namespace QuizMan.BLL.DataTransferObjects.AnswerModel
{
    public class ChooseModel
    {
        public int ChooseId { get; set; }
        public string IsChecked { get; set; }
    }
}