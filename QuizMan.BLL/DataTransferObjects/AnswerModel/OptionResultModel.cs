﻿namespace QuizMan.BLL.DataTransferObjects.AnswerModel
{
    public class OptionResultModel
    {
        public string Text { get; set; }
        public bool IsAnswer { get; set; }
        public bool UserAnswer { get; set; }
    }
}
