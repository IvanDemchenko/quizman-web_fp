﻿using System;
using System.Collections.Generic;
using QuizMan.BLL.Common;

namespace QuizMan.BLL.DataTransferObjects
{
    public class TestModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int TimeToPass { get; set; }
        public DateTime CreationDate { get; set; }
        public int TestPassingPercentage { get; set; }
        public TestState TestState { get; set; }
        public bool IsActive { get; set; }
        public bool Deprecated { get; set; }
        public int CategoryId { get; set; }
        public virtual List<QuestionModel> Questions { get; set; }
    }
}
