﻿namespace QuizMan.BLL.DataTransferObjects
{
    public class ApplicationUserModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Git { get; set; }
        public string Role { get; set; }
        public int Age { get; set; }
    }
}
