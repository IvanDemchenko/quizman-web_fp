﻿namespace QuizMan.BLL.DataTransferObjects
{
    public class RoleModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
