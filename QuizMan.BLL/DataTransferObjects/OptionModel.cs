﻿namespace QuizMan.BLL.DataTransferObjects
{
    public class OptionModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string IsAnswer { get; set; }
    }
}
