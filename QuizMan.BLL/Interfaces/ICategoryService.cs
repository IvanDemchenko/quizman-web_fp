﻿using System.Collections.Generic;
using System.Threading.Tasks;
using QuizMan.BLL.DataTransferObjects;

namespace QuizMan.BLL.Interfaces
{
    /// <summary>
    /// Category service interface
    /// </summary>
    public interface ICategoryService
    {
        /// <summary>
        /// Method of model validation.
        /// </summary>
        /// <param name="model">Category model</param>
        /// <returns></returns>
        IDictionary<string, string> ValidateCategory(CategoryModel model);

        /// <summary>
        /// Returns all categories with short description 
        /// </summary>
        /// <returns></returns>
        IEnumerable<CategoryModel> GetAllCategoriesWithShortDescription();

        /// <summary>
        /// Returns the category by id
        /// </summary>
        /// <param name="id">Category primary key</param>
        /// <returns></returns>
        Task<CategoryModel> GetCategoryByIdAsync(int id);

        /// <summary>
        /// Returns the category by id with tests
        /// </summary>
        /// <param name="id">Category primary key</param>
        /// <returns></returns>
        Task<CategoryModel> GetCategoryWithDetailsByIdAsync(int id);

        /// <summary>
        /// Adds or Updates Category
        /// </summary>
        /// <param name="categoryModel">Category model</param>
        /// <returns></returns>
        Task<int> AddOrUpdateCategory(CategoryModel categoryModel);

        /// <summary>
        /// Deletes the category by category id
        /// </summary>
        /// <param name="categoryId">Category primary key</param>
        /// <returns></returns>
        Task DeleteCategory(int categoryId);

        /// <summary>
        /// Returns number of category
        /// </summary>
        /// <returns></returns>
        int CategoriesNumber();
    }
}
