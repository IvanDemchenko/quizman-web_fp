﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuizMan.BLL.Common;
using QuizMan.BLL.DataTransferObjects;
using QuizMan.BLL.DataTransferObjects.AnswerModel;

namespace QuizMan.BLL.Interfaces
{
    /// <summary>
    /// Test service interface
    /// </summary>
    public interface ITestService
    {
        /// <summary>
        /// Method of model validation.
        /// </summary>
        /// <param name="model">Test model</param>
        /// <returns></returns>
        IDictionary<string, string> ValidateTestModel(TestModel model);

        /// <summary>
        /// Returns tests with user progress by user id and category id
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <param name="categoryId">Category primary key</param>
        /// <returns></returns>
        IEnumerable<TestModel> GetTestsWithProgress(string userId, int? categoryId = null);

        /// <summary>
        /// Returns tests from database
        /// </summary>
        /// <param name="categoryId">Category primary key</param>
        /// <returns></returns>
        IEnumerable<TestModel> GetTests(int? categoryId = null);

        /// <summary>
        /// Returns tests by category id
        /// </summary>
        /// <param name="id">Category primary key</param>
        /// <returns></returns>
        IEnumerable<TestModel> GetTestsByCategoryId(int id);

        /// <summary>
        /// Deletes test by id
        /// </summary>
        /// <param name="id">Test primary key</param>
        /// <returns></returns>
        Task DeleteTestById(int id);

        /// <summary>
        /// Returns test with details
        /// </summary>
        /// <param name="id">Test primary key</param>
        /// <returns></returns>
        Task<TestModel> GetTestWithDetailsByIdAsync(int id);

        /// <summary>
        /// Returns test information by id
        /// </summary>
        /// <param name="id">Test primary key</param>
        /// <returns></returns>
        Task<TestModel> GetTestById(int id);

        /// <summary>
        /// Returns test with user progress by id
        /// </summary>
        /// <param name="id">Test primary key</param>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        Task<TestModel> GetTestByIdWithProgress(int id, string userId);

        /// <summary>
        /// Adds or updates test
        /// </summary>
        /// <param name="testModel">Test model</param>
        /// <returns></returns>
        Task<int> AddOrUpdateTest(TestModel testModel);

        /// <summary>
        /// Returns the detailed result of the passed test. 
        /// </summary>
        /// <param name="testId">Test primary key</param>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        Task<DetailedTestResultModel> GetDetailedResult(int testId, string userId);

        /// <summary>
        /// Returns test quick result
        /// </summary>
        /// <param name="testId">Test primary key</param>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        Task<QuickResultModel> GetQuickResult(int testId, string userId);

        /// <summary>
        /// Adds user answer to database and calculates quick result
        /// </summary>
        /// <param name="testAnswersModel">Test answer model</param>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        Task AddUserAnswer(TestAnswersModel testAnswersModel, string userId);

        /// <summary>
        /// Registers that the user has started the test.
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <param name="testId">Test primary key</param>
        /// <returns></returns>
        Task<DateTime> RegisterUserToTest(string userId, int testId);

        /// <summary>
        /// Checks the test status by user. 
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <param name="testId">Test primary key</param>
        /// <returns></returns>
        Task<TestStatusModel> CheckTestStatus(string userId, int testId);

        /// <summary>
        /// Changes test status (active/ not active)
        /// </summary>
        /// <param name="testId">Test primary key</param>
        /// <returns></returns>
        Task ChangeTestStatus(int testId);

        /// <summary>
        /// Returns asynchronously tests quick results by user
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        IEnumerable<QuickResultModel> GetTestQuickResultsByUserId(string userId);

        /// <summary>
        /// Returns tests quick results by user
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        Task<IEnumerable<QuickResultModel>> GetTestQuickResultsByUserIdAsync(string userId);

        /// <summary>
        /// Returns the top 3 users by the number of passed tests. 
        /// </summary>
        /// <returns></returns>
        IEnumerable<TopUserModel> GetTopUsers();

        /// <summary>
        /// Returns number of tests
        /// </summary>
        /// <returns></returns>
        int TestsNumber();
    }
}
