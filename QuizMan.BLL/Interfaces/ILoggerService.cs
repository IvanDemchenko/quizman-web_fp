﻿using System.Collections.Generic;
using QuizMan.BLL.DataTransferObjects;

namespace QuizMan.BLL.Interfaces
{
    /// <summary>
    /// Logger service interface
    /// </summary>
    public interface ILoggerService
    {
        /// <summary>
        /// Writes log to database
        /// </summary>
        /// <param name="exceptionDetailModel">Exception detail model</param>
        void WriteLog(ExceptionDetailModel exceptionDetailModel);

        /// <summary>
        /// Returns all logs
        /// </summary>
        /// <returns></returns>
        IEnumerable<ExceptionDetailModel> GetAllLogs();
    }
}
