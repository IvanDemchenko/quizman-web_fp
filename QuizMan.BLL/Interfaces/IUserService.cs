﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using QuizMan.BLL.DataTransferObjects;
using QuizMan.BLL.DataTransferObjects.UserManage;
using QuizMan.BLL.Infrastructure;

namespace QuizMan.BLL.Interfaces
{
    /// <summary>
    /// User Service interface
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Creates new user
        /// </summary>
        /// <param name="userModel">User model</param>
        /// <returns></returns>
        Task<OperationDetails> Create(UserModel userModel);

        /// <summary>
        /// Authenticates user
        /// </summary>
        /// <param name="userModel">User model</param>
        /// <returns></returns>
        Task<ClaimsIdentity> Authenticate(UserModel userModel);

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="id">User primary key</param>
        /// <returns></returns>
        Task<ApplicationUserModel> GetByIdAsync(string id);

        /// <summary>
        /// Returns all users
        /// </summary>
        /// <returns></returns>
        IEnumerable<ApplicationUserModel> GetAllUsers();

        /// <summary>
        /// Returns user roles
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        Task<List<string>> GetUserRoles(string userId);

        /// <summary>
        /// Returns all roles
        /// </summary>
        /// <returns></returns>
        List<RoleModel> GetAllRoles();

        /// <summary>
        /// Checks if the user has a role.  
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <param name="name">Role name</param>
        /// <returns></returns>
        Task<bool> UserIsInRole(string userId, string name);

        /// <summary>
        /// Updates user roles
        /// </summary>
        /// <param name="editUserRolesModel">User roles model</param>
        /// <returns></returns>
        Task UpdateUserRoles(EditUserRolesModel editUserRolesModel);

        /// <summary>
        /// Checks if the user is banned.
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        Task<bool> IsLockedOutAsync(string userId);

        /// <summary>
        /// Returns lockout end date
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        Task<DateTimeOffset> GetLockoutEndDateAsync(string userId);

        /// <summary>
        /// Sets lockout date
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <param name="days">Number of days</param>
        /// <returns></returns>
        Task<OperationDetails> SetLockOutDate(string userId, int days);

        /// <summary>
        /// Removes the ban from the user 
        /// </summary>
        /// <param name="userId">User primary key</param>
        /// <returns></returns>
        Task<OperationDetails> UnBan(string userId);

        /// <summary>
        /// Updates user profile
        /// </summary>
        /// <param name="applicationUserModel"></param>
        /// <returns></returns>
        Task UpdateUserProfile(ApplicationUserModel applicationUserModel);

        /// <summary>
        /// Changes user password
        /// </summary>
        /// <param name="changePasswordModel">Password model</param>
        /// <returns></returns>
        Task ChangePassword(ChangePasswordModel changePasswordModel);

        /// <summary>
        /// Returns number of users
        /// </summary>
        /// <returns></returns>
        int UsersNumber();
    }
}
