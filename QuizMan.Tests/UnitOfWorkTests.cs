﻿using NUnit.Framework;
using QuizMan.DAL.Repositories;

namespace QuizMan.Tests
{
    [TestFixture]
    internal class UnitOfWorkTests
    {
        [Test]
        public void GetRepository_ReturnsProperRepository()
        {
            var uow = new UnitOfWork("ApplicationDbContext");
            var result = uow.TestRepository;
            Assert.AreSame(result, uow.TestRepository);
        }
    }
}
