﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Moq;
using NUnit.Framework;
using QuizMan.DAL.EntityFramework;
using QuizMan.DAL.Repositories;
using QuizMan.Tests.Infrastructure;
using QuizMan.Tests.Models;

namespace QuizMan.Tests
{
    [TestFixture]
    internal class RepositoryTests
    {
        [Test]
        public void Create_TestClassObjectPassed_ProperMethodCalled()
        {
            var testObj = new TestClass();
            var context = new Mock<ApplicationDbContext>("ApplicationDbContext");
            var dbSetMock = new Mock<DbSet<TestClass>>();
            context.Setup(x => x.Set<TestClass>()).Returns(dbSetMock.Object);
            dbSetMock.Setup(x => x.Add(It.IsAny<TestClass>())).Returns(testObj);

            var repository = new Repository<TestClass>(context.Object);
            repository.Add(testObj);
            context.Verify(x => x.Set<TestClass>());
            dbSetMock.Verify(x => x.Add(It.Is<TestClass>(y => y == testObj)));
        }

        [Test]
        public void Delete_TestClassObjectPassed_ProperMethodCalled()
        {
            var testObj = new TestClass();
            var context = new Mock<ApplicationDbContext>("ApplicationDbContext");
            var dbSetMock = new Mock<DbSet<TestClass>>();
            context.Setup(x => x.Set<TestClass>()).Returns(dbSetMock.Object);
            dbSetMock.Setup(x => x.Remove(It.IsAny<TestClass>())).Returns(testObj);

            var repository = new Repository<TestClass>(context.Object);
            repository.Delete(testObj);
            context.Verify(x => x.Set<TestClass>());
            dbSetMock.Verify(x => x.Remove(It.Is<TestClass>(y => y == testObj)));
        }

        [Test]
        public void GetById_TestClassObjectPassed_ProperMethodCalled()
        {
            var testObj = new TestClass();
            var context = new Mock<ApplicationDbContext>("ApplicationDbContext");
            var dbSetMock = new Mock<DbSet<TestClass>>();
            context.Setup(x => x.Set<TestClass>()).Returns(dbSetMock.Object);
            dbSetMock.Setup(x => x.Find(It.IsAny<int>())).Returns(testObj);

            var repository = new Repository<TestClass>(context.Object);
            repository.GetByIdAsync(1).Wait();

            context.Verify(x => x.Set<TestClass>());
            dbSetMock.Verify(x => x.FindAsync(It.IsAny<int>()));
        }

        [Test]
        public void GetAll_TestClassObjectPassed_ProperMethodCalled()
        {
            var testObj = new TestClass { Id = 1 };
            var testList = new List<TestClass> { testObj };

            var dbSetMock = new Mock<DbSet<TestClass>>();
            dbSetMock.As<IQueryable<TestClass>>().Setup(x => x.Provider).Returns(testList.AsQueryable().Provider);
            dbSetMock.As<IQueryable<TestClass>>().Setup(x => x.Expression).Returns(testList.AsQueryable().Expression);
            dbSetMock.As<IQueryable<TestClass>>().Setup(x => x.ElementType).Returns(testList.AsQueryable().ElementType);
            dbSetMock.As<IQueryable<TestClass>>().Setup(x => x.GetEnumerator())
                .Returns(testList.AsQueryable().GetEnumerator());

            var context = new Mock<ApplicationDbContext>("ApplicationDbContext");

            context.Setup(x => x.Set<TestClass>()).Returns(dbSetMock.Object);

            var repository = new Repository<TestClass>(context.Object);
            var result = repository.GetAll();

            Assert.AreEqual(testList, result.ToList());
        }

        [Test]
        public void GetAllAsync_TestClassObjectPassed_ProperMethodCalled()
        {
            var testObj = new TestClass { Id = 1 };
            var testList = new List<TestClass> { testObj };

            var dbSetMock = new Mock<DbSet<TestClass>>();
            dbSetMock.As<IQueryable<TestClass>>().Setup(x => x.Provider).Returns(new TestDbAsyncQueryProvider<TestClass>(testList.AsQueryable().Provider));
            dbSetMock.As<IQueryable<TestClass>>().Setup(x => x.Expression).Returns(testList.AsQueryable().Expression);
            dbSetMock.As<IQueryable<TestClass>>().Setup(x => x.ElementType).Returns(testList.AsQueryable().ElementType);
            dbSetMock.As<IQueryable<TestClass>>().Setup(x => x.GetEnumerator())
                .Returns(testList.AsQueryable().GetEnumerator());
            dbSetMock.As<IDbAsyncEnumerable<TestClass>>().Setup(x => x.GetAsyncEnumerator())
                .Returns(new TestDbAsyncEnumerator<TestClass>(testList.GetEnumerator()));

            var context = new Mock<ApplicationDbContext>("ApplicationDbContext");

            context.Setup(x => x.Set<TestClass>()).Returns(dbSetMock.Object);

            var repository = new Repository<TestClass>(context.Object);

            var resultAsync = repository.GetAll();

            Assert.AreEqual(testList, resultAsync.ToList());
        }

        [Test]
        public void Get_TestClassObjectPassed_ProperMethodCalled()
        {
            var testObj = new TestClass { Id = 1 };
            var testList = new List<TestClass> { testObj };

            var dbSetMock = new Mock<DbSet<TestClass>>();
            dbSetMock.As<IQueryable<TestClass>>().Setup(x => x.Provider).Returns(testList.AsQueryable().Provider);
            dbSetMock.As<IQueryable<TestClass>>().Setup(x => x.Expression).Returns(testList.AsQueryable().Expression);
            dbSetMock.As<IQueryable<TestClass>>().Setup(x => x.ElementType).Returns(testList.AsQueryable().ElementType);
            dbSetMock.As<IQueryable<TestClass>>().Setup(x => x.GetEnumerator())
                .Returns(testList.AsQueryable().GetEnumerator());

            var context = new Mock<ApplicationDbContext>("ApplicationDbContext");

            context.Setup(x => x.Set<TestClass>()).Returns(dbSetMock.Object);

            var repository = new Repository<TestClass>(context.Object);
            var result = repository.Get(x => x.Id == 1);

            Assert.AreEqual(testList, result.ToList());
        }

    }
}
