﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using QuizMan.DAL.Entities;

namespace QuizMan.DAL.Interfaces
{
    /// <summary>
    /// Repository interface
    /// </summary>
    /// <typeparam name="TEntity">Type of repository entity</typeparam>
    public interface IRepository<TEntity> where TEntity: BaseEntity
    {
        /// <summary>
        /// Returns all entities
        /// </summary>
        /// <returns></returns>
        IQueryable<TEntity> GetAll();

        /// <summary>
        /// Returns entity by id
        /// </summary>
        /// <param name="id">Entity primary key</param>
        /// <returns></returns>
        Task<TEntity> GetByIdAsync(int id);

        /// <summary>
        /// Adds new entity to database
        /// </summary>
        /// <param name="entity">Entity model</param>
        void Add(TEntity entity);

        /// <summary>
        /// Updates entity
        /// </summary>
        /// <param name="entity">Entity model</param>
        void Update(TEntity entity);

        /// <summary>
        /// Deletes entity
        /// </summary>
        /// <param name="entity">Entity model</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Deletes entity by id
        /// </summary>
        /// <param name="id">Entity primary key</param>
        /// <returns></returns>
        Task DeleteByIdAsync(int id);

        /// <summary>
        /// Returns number of entities 
        /// </summary>
        /// <returns></returns>
        int Count();

        /// <summary>
        /// Returns the entities by filter and order. 
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <param name="orderBy">Order</param>
        /// <returns></returns>
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null);

        /// <summary>
        /// Returns the entities by filter and order including related tables
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <param name="orderBy">Order</param>
        /// <param name="includeProperties">Related tables</param>
        /// <returns></returns>
        IEnumerable<TEntity> GetWithInclude(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            params Expression<Func<TEntity, object>>[] includeProperties);

        /// <summary>
        /// Returns the entities asynchronously including related tables
        /// </summary>
        /// <param name="includeProperties">Related tables</param>
        /// <returns></returns>
        Task<IEnumerable<TEntity>> GetWithIncludeAsync(params Expression<Func<TEntity, object>>[] includeProperties);

        /// <summary>
        /// Returns the entities asynchronously by filter and order including related tables
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <param name="orderBy">Order</param>
        /// <param name="includeProperties">Related tables</param>
        /// <returns></returns>
        Task<IEnumerable<TEntity>> GetWithIncludeAsync(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            params Expression<Func<TEntity, object>>[] includeProperties);
    }
}
