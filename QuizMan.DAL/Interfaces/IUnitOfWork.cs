﻿using System.Threading.Tasks;
using QuizMan.DAL.Entities;
using QuizMan.DAL.Entities.Log;
using QuizMan.DAL.Identity;

namespace QuizMan.DAL.Interfaces
{
    /// <summary>
    /// Unit of work interface
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Test repository
        /// </summary>
        ITestRepository TestRepository { get; }

        /// <summary>
        /// Category repository
        /// </summary>
        IRepository<Category> CategoryRepository { get; }

        /// <summary>
        /// Test status repository
        /// </summary>
        IRepository<TestStatus> TestStatusRepository { get; }

        /// <summary>
        /// User answer repository
        /// </summary>
        IRepository<Answer> AnswerRepository { get; }

        /// <summary>
        /// Quick result repository
        /// </summary>
        IRepository<QuickResult> QuickResultRepository { get; }

        /// <summary>
        /// Exception details repository
        /// </summary>
        IRepository<ExceptionDetail> ExceptionDetailRepository { get; }

        /// <summary>
        /// Identity user manager
        /// </summary>
        ApplicationUserManager UserManager { get; }

        /// <summary>
        /// Client profile manager
        /// </summary>
        IClientManager ClientManager { get; }

        /// <summary>
        /// Identity role manager
        /// </summary>
        ApplicationRoleManager RoleManager { get; }

        /// <summary>
        /// Saves changes
        /// </summary>
        void Save();

        /// <summary>
        /// Save changes asynchronously
        /// </summary>
        /// <returns></returns>
        Task SaveAsync();
    }
}
