﻿using System;
using QuizMan.DAL.Entities.Identity;

namespace QuizMan.DAL.Interfaces
{
    /// <summary>
    /// Client manager interface
    /// </summary>
    public interface IClientManager:IDisposable
    {
        /// <summary>
        /// Creates a new user profile. 
        /// </summary>
        /// <param name="item">Client profile model</param>
        void Create(ClientProfile item);
    }
}
