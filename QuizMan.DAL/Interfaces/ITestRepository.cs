﻿using QuizMan.DAL.Entities;

namespace QuizMan.DAL.Interfaces
{
    /// <summary>
    /// Test repository interface
    /// </summary>
    public interface ITestRepository :IRepository<Test>
    {
        /// <summary>
        /// Updates test
        /// </summary>
        /// <param name="test">Test model</param>
        void UpdateTestWithDetails(Test test);
    }
}
