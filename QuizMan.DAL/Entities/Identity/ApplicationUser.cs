﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace QuizMan.DAL.Entities.Identity
{
    public class ApplicationUser:IdentityUser
    {
        public virtual ClientProfile ClientProfile { get; set; }
    }
}
