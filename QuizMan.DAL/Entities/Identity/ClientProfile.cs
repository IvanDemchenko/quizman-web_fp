﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuizMan.DAL.Entities.Identity
{
    public class ClientProfile
    {
        [Key]
        [ForeignKey("ApplicationUser")]
        public string Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(75)]
        public string Surname { get; set; }

        [MaxLength(100)]
        public string Git { get; set; }
        public int Age { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
