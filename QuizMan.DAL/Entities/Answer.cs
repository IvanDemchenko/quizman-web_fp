﻿using System.ComponentModel.DataAnnotations;

namespace QuizMan.DAL.Entities
{
    public class Answer:BaseEntity
    {
        [MaxLength(500)]
        public string TextAnswer { get; set; }

        public int StartedTestId { get; set; }
        public TestStatus StartedTest { get; set; }
        public int QuestionId { get; set; }
        public Question Question { get; set; }

        public int? OptionId { get; set; }
        public Option Option { get; set; }
    }
}
