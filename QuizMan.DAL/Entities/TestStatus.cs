﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using QuizMan.DAL.Entities.Identity;

namespace QuizMan.DAL.Entities
{
    public class TestStatus:BaseEntity
    {
        public DateTime StartedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        [ForeignKey("ApplicationUser")]
        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        public int? TestId { get; set; }
        public Test Test { get; set; }

        public ICollection<Answer> Answers { get; set; }
        public QuickResult QuickResult { get; set; }
    }
}
