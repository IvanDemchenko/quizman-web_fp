﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace QuizMan.DAL.Entities
{
    public class Category:BaseEntity
    {
        [Required]
        [MaxLength(75)]
        public string Title { get; set; }
        [Required]
        [MaxLength(1000)]
        public string Description { get; set; }

        public DateTime CreationDate { get; set; }

        public ICollection<Test> Tests { get; set; }
    }
}
