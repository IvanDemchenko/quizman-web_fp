﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QuizMan.DAL.Entities
{
    public class QuickResult:BaseEntity
    {
        [Key]
        [ForeignKey("TestStatus")]
        public new int Id { get; set; }
        public int TotalPoints { get; set; }
        public int EarnedPoints { get; set; }
        public bool IsPassed { get; set; }

        public TestStatus TestStatus { get; set; }
    }
}
