﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace QuizMan.DAL.Entities
{
    public class Option:BaseEntity
    {
        [Required]
        [MaxLength(500)]
        public string Text { get; set; }
        public bool IsAnswer { get; set; }

        public int QuestionId { get; set; }
        public Question Question { get; set; }

        public ICollection<Answer> Answers { get; set; }
    }
}
