﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace QuizMan.DAL.Entities
{
    public class Test:BaseEntity
    {
        [Required]
        [MaxLength(75)]
        public string Title { get; set; }
        [Required]
        [MaxLength(1000)]
        public string Description { get; set; }
        [Required]
        public int TimeToPass { get; set; }
        public DateTime CreationDate { get; set; }
        [Required]
        public int TestPassingPercentage { get; set; }
        public bool IsActive { get; set; }
        public bool Deprecated { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public ICollection<Question> Questions { get; set; }
    }
}
