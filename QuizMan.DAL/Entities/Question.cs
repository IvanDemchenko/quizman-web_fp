﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace QuizMan.DAL.Entities
{
    public class Question:BaseEntity
    {
        [Required]
        [MaxLength(500)]
        public string Text { get; set; }
        [Required]
        [MaxLength(20)]
        public string Type { get; set; }
        public int Score { get; set; }


        public int TestId { get; set; }
        public Test Test { get; set; }
        public ICollection<Option> Options { get; set; }
        public ICollection<Answer> Answers { get; set; }
    }
}
