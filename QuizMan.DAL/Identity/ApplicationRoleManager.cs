﻿using Microsoft.AspNet.Identity;
using QuizMan.DAL.Entities.Identity;

namespace QuizMan.DAL.Identity
{
    public class ApplicationRoleManager : RoleManager<ApplicationRole>
    {
        public ApplicationRoleManager(IRoleStore<ApplicationRole, string> store):base(store) { }
    }
}
