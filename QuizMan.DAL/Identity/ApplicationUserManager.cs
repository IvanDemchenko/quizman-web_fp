﻿using Microsoft.AspNet.Identity;
using QuizMan.DAL.Entities.Identity;

namespace QuizMan.DAL.Identity
{
    public class ApplicationUserManager:UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store) : base(store) { }
    }
}
