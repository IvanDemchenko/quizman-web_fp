﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using QuizMan.DAL.Entities;
using QuizMan.DAL.EntityFramework;
using QuizMan.DAL.Interfaces;

namespace QuizMan.DAL.Repositories
{
    /// <summary>
    /// A basic repository that contains the main operations with data.
    /// </summary>
    /// <typeparam name="TEntity">Type of repository entity</typeparam>
    public class Repository<TEntity>:IRepository<TEntity> where TEntity:BaseEntity
    {
        protected readonly ApplicationDbContext Context;
        protected readonly DbSet<TEntity> DbSet;

        /// <summary>
        /// Initializes an instance of the repository
        /// </summary>
        /// <param name="context">Database context</param>
        public Repository(ApplicationDbContext context)
        {
            Context = context;
            DbSet = context.Set<TEntity>();
        }

        /// <summary>
        /// Returns all entities
        /// </summary>
        /// <returns></returns>
        public IQueryable<TEntity> GetAll()
        {
            return DbSet;
        }

        /// <summary>
        /// Returns entity by id
        /// </summary>
        /// <param name="id">Entity primary key</param>
        /// <returns></returns>
        public Task<TEntity> GetByIdAsync(int id)
        {
            return DbSet.FindAsync(id);
        }

        /// <summary>
        /// Adds new entity to database
        /// </summary>
        /// <param name="entity">Entity model</param>
        public void Add(TEntity entity)
        {
            DbSet.Add(entity);
        }

        /// <summary>
        /// Updates entity
        /// </summary>
        /// <param name="entity">Entity model</param>
        public void Update(TEntity entity)
        {
            DbSet.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// Deletes entity
        /// </summary>
        /// <param name="entity">Entity model</param>
        public void Delete(TEntity entity)
        {
            DbSet.Remove(entity);
        }

        /// <summary>
        /// Deletes entity by id
        /// </summary>
        /// <param name="id">Entity primary key</param>
        /// <returns></returns>
        public async Task DeleteByIdAsync(int id)
        {
            var entity = await GetByIdAsync(id);
            DbSet.Remove(entity);
        }

        /// <summary>
        /// Returns number of entities 
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            return DbSet.Distinct().AsNoTracking().Select(x => new {x.Id}).Count();
        }

        /// <summary>
        /// Returns the entities by filter and order. 
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <param name="orderBy">Order</param>
        /// <returns></returns>
        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            IQueryable<TEntity> query = DbSet;
            if (filter != null)
                query = query.Where(filter);
            return orderBy != null ? orderBy(query).ToList() : query.ToList();
        }

        /// <summary>
        /// Returns the entities by filter and order including related tables
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <param name="orderBy">Order</param>
        /// <param name="includeProperties">Related tables</param>
        /// <returns></returns>
        public IEnumerable<TEntity> GetWithInclude(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var query = Include(includeProperties);
            if (filter != null)
                query = query.Where(filter);
            return orderBy != null ? orderBy(query).ToList() : query.ToList();
        }

        /// <summary>
        /// Returns the entities asynchronously including related tables
        /// </summary>
        /// <param name="includeProperties">Related tables</param>
        /// <returns></returns>
        public async Task<IEnumerable<TEntity>> GetWithIncludeAsync(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return await Include(includeProperties).ToListAsync();
        }

        /// <summary>
        /// Returns the entities asynchronously by filter and order including related tables
        /// </summary>
        /// <param name="filter">Filter</param>
        /// <param name="orderBy">Order</param>
        /// <param name="includeProperties">Related tables</param>
        /// <returns></returns>
        public async Task<IEnumerable<TEntity>> GetWithIncludeAsync(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var query = Include(includeProperties);
            if (filter != null)
                query = query.Where(filter);
            return orderBy != null ? await orderBy(query).ToListAsync() : await query.ToListAsync();
        }

        private IQueryable<TEntity> Include(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = DbSet;
            return includeProperties
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }
    }
}
