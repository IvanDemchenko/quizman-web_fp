﻿using System;
using QuizMan.DAL.Entities.Identity;
using QuizMan.DAL.EntityFramework;
using QuizMan.DAL.Interfaces;

namespace QuizMan.DAL.Repositories
{
    /// <summary>
    /// Manager on work with client profile. 
    /// </summary>
    public class ClientManager : IClientManager
    {
        /// <summary>
        /// Database context 
        /// </summary>
        private readonly ApplicationDbContext _context;

        /// <summary>
        /// Initialize an instance of the client manager
        /// </summary>
        /// <param name="context">Database context</param>
        public ClientManager(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Creates a new user profile. 
        /// </summary>
        /// <param name="item">Client profile model</param>
        public void Create(ClientProfile item)
        {
            _context.ClientProfiles.Add(item);
            _context.SaveChanges();
        }

        private bool _disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (this._disposed) return;
            if (disposing)
                _context.Dispose();
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
