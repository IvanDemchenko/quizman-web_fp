﻿using System.Data.Entity;
using System.Linq;
using QuizMan.DAL.Entities;
using QuizMan.DAL.EntityFramework;
using QuizMan.DAL.Interfaces;

namespace QuizMan.DAL.Repositories
{
    /// <summary>
    /// Represent a test repository
    /// </summary>
    public class TestRepository:Repository<Test>, ITestRepository
    {
        /// <summary>
        /// Initialize an instance of the test repository
        /// </summary>
        /// <param name="context">Database context</param>
        public TestRepository(ApplicationDbContext context) : base(context) { }

        /// <summary>
        /// Updates test
        /// </summary>
        /// <param name="test">Test model</param>
        public void UpdateTestWithDetails(Test test)
        {
            var missingRows = Context.Questions.Where(x => x.TestId == test.Id);
            Context.Questions.RemoveRange(missingRows);
            Context.Entry(test).State = EntityState.Modified;
        }
    }
}
