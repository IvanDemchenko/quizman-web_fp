﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using QuizMan.DAL.Entities;
using QuizMan.DAL.Entities.Identity;
using QuizMan.DAL.Entities.Log;
using QuizMan.DAL.EntityFramework;
using QuizMan.DAL.Identity;
using QuizMan.DAL.Interfaces;

namespace QuizMan.DAL.Repositories
{
    /// <summary>
    /// Represent a unit of work
    /// </summary>
    public class UnitOfWork:IUnitOfWork,IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;

        private ITestRepository _testRepository;
        private IRepository<Category> _categoryRepository;
        private IRepository<TestStatus> _testStatusRepository;
        private IRepository<Answer> _userAnswerRepository;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        private IClientManager _clientManager;
        private IRepository<QuickResult> _quickResultRepository;
        private IRepository<ExceptionDetail> _exceptionDetailRepository;

        /// <summary>
        /// Test repository
        /// </summary>
        public ITestRepository TestRepository => _testRepository ??= new TestRepository(_context);

        /// <summary>
        /// Category repository
        /// </summary>
        public IRepository<Category> CategoryRepository => _categoryRepository ??= new Repository<Category>(_context);

        /// <summary>
        /// Test status repository
        /// </summary>
        public IRepository<TestStatus> TestStatusRepository => _testStatusRepository ??= new Repository<TestStatus>(_context);

        /// <summary>
        /// User answer repository
        /// </summary>
        public IRepository<Answer> AnswerRepository => _userAnswerRepository ??= new Repository<Answer>(_context);

        /// <summary>
        /// Quick result repository
        /// </summary>
        public IRepository<QuickResult> QuickResultRepository => _quickResultRepository ??= new Repository<QuickResult>(_context);

        /// <summary>
        /// Identity user manager
        /// </summary>
        public ApplicationUserManager UserManager => _userManager ??= new ApplicationUserManager(new UserStore<ApplicationUser>(_context));

        /// <summary>
        /// Identity role manager
        /// </summary>
        public ApplicationRoleManager RoleManager => _roleManager ??= new ApplicationRoleManager(new RoleStore<ApplicationRole>(_context));

        /// <summary>
        /// Client profile manager
        /// </summary>
        public IClientManager ClientManager => _clientManager ??= new ClientManager(_context);

        /// <summary>
        /// Exception details repository
        /// </summary>
        public IRepository<ExceptionDetail> ExceptionDetailRepository => _exceptionDetailRepository ??= new Repository<ExceptionDetail>(_context);

        /// <summary>
        /// Initializes an instance of the repository
        /// </summary>
        /// <param name="connectionString">Database connection string</param>
        public UnitOfWork(string connectionString) => _context = new ApplicationDbContext(connectionString);

        /// <summary>
        /// Saves changes
        /// </summary>
        public void Save() => _context.SaveChanges();

        /// <summary>
        /// Save changes asynchronously
        /// </summary>
        /// <returns></returns>
        public async Task SaveAsync() => await _context.SaveChangesAsync();

        protected virtual void Dispose(bool disposing)
        {
            if (this._disposed) return;
            if (disposing)
                _context.Dispose();
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
