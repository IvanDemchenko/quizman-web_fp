﻿using System;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using QuizMan.DAL.Entities;
using QuizMan.DAL.Entities.Identity;
using QuizMan.DAL.Entities.Log;
using QuizMan.DAL.Identity;

namespace QuizMan.DAL.EntityFramework
{
    public class ApplicationDbContext: IdentityDbContext<ApplicationUser>
    {
        static ApplicationDbContext()
        {
            Database.SetInitializer(new DataBaseInitializer());
        }
        public ApplicationDbContext(string connectionString) : base(connectionString) { }
        public IDbSet<ClientProfile> ClientProfiles { get; set; }
        public IDbSet<Category> Categories { get; set; }
        public IDbSet<Test> Tests { get; set; }
        public DbSet<Question> Questions { get; set; }
        public IDbSet<Option> Options { get; set; }
        public IDbSet<Answer> Answers { get; set; }
        public IDbSet<TestStatus> TestStatuses { get; set; }
        public IDbSet<QuickResult> QuickResults { get; set; }
        public IDbSet<ExceptionDetail> ExceptionDetails { get; set; }
    }
    public class DataBaseInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
            var roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(context));

            var role1 = new ApplicationRole{Name = "admin"};
            var role2 = new ApplicationRole{Name = "editor"};
            var role3 = new ApplicationRole{Name = "user"};

            roleManager.Create(role1);
            roleManager.Create(role2);
            roleManager.Create(role3);

            var admin = new ApplicationUser {Email = "raycast@mail.com", UserName = "raycast@mail.com"};
            var result = userManager.Create(admin, "Dizel_1");
            if (result.Succeeded)
                userManager.AddToRole(admin.Id, role1.Name);

            var adminProfile = new ClientProfile {ApplicationUser = admin, FirstName = "Ivan", Surname = "Demchenko", Git = "Some url", Age = 23};
            context.ClientProfiles.Add(adminProfile);


            var category0 = new Category
            {
                Title = ".NET",
                Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                CreationDate = DateTime.Now
            };

            var category1 = new Category { Title = "Unity", Description = "Unity quiz",CreationDate = DateTime.Now};
            var category2 = new Category
            {
                Title = "ASP.NET MVC",
                Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                CreationDate = DateTime.Now
            };
            var category3 = new Category { Title = "SQL", Description = "SQL quiz", CreationDate = DateTime.Now};
            var category4 = new Category { Title = "OPEN GL", Description = "OPEN GL quiz", CreationDate = DateTime.Now};
            var category5 = new Category
            {
                Title = "ASP.NET Core",
                Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                CreationDate = DateTime.Now
            };
            var category6 = new Category { Title = "OOP", Description = "OOP quiz", CreationDate = DateTime.Now};
            var category7 = new Category { Title = "Patterns", Description = "Patterns quiz", CreationDate = DateTime.Now};
            var category8 = new Category { Title = "JS", Description = "JS quiz", CreationDate = DateTime.Now};
            var category9 = new Category { Title = "Angular", Description = "Angular quiz", CreationDate = DateTime.Now};
            var category10 = new Category { Title = "React", Description = "React quiz", CreationDate = DateTime.Now};
            var category11 = new Category { Title = "Other", Description = "Other quiz", CreationDate = DateTime.Now};
            context.Categories.Add(category0);
            context.Categories.Add(category1);
            context.Categories.Add(category2);
            context.Categories.Add(category3);
            context.Categories.Add(category4);
            context.Categories.Add(category5);
            context.Categories.Add(category6);
            context.Categories.Add(category7);
            context.Categories.Add(category8);
            context.Categories.Add(category9);
            context.Categories.Add(category10);
            context.Categories.Add(category11);

            var test0 = new Test
            {
                IsActive = true,
                Deprecated = false,
                Title = "C# Basics",
                Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                TimeToPass = 60,
                Category = category0,
                CreationDate = new DateTime(2020,09,02),
                TestPassingPercentage = 90
            };

            var test1 = new Test
            {
                IsActive = false,
                Deprecated = false,
                Title = "ASP Basics",
                Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                TimeToPass = 60,
                Category = category0,
                CreationDate = new DateTime(2020, 09, 02),
                TestPassingPercentage = 95
            };

            var test2 = new Test
            {
                IsActive = true,
                Deprecated = false,
                Title = "Unity Basics",
                Description = "Some description Unity",
                TimeToPass = 60,
                Category = category1,
                CreationDate = new DateTime(2020, 09, 02),
                TestPassingPercentage = 80
            };

            var test3 = new Test
            {
                IsActive = true,
                Deprecated = false,
                Title = "Animation Basics",
                Description = "Some description Animation",
                TimeToPass = 60, Category = category1,
                CreationDate = new DateTime(2020, 09, 02),
                TestPassingPercentage = 92
            };

            var test4 = new Test
            {
                IsActive = true,
                Deprecated = false,
                Title = "Test5",
                Description = "Some description Test 5",
                TimeToPass = 60,
                Category = category2,
                CreationDate = new DateTime(2020, 09, 02),
                TestPassingPercentage = 90
            };

            var test5 = new Test
            {
                IsActive = true,
                Deprecated = false,
                Title = "Test6",
                Description = "Some description Test 6",
                TimeToPass = 60,
                Category = category2,
                CreationDate = new DateTime(2020, 09, 02),
                TestPassingPercentage = 95
            };
            
            context.Tests.Add(test0);
            context.Tests.Add(test1);
            context.Tests.Add(test2);
            context.Tests.Add(test3);
            context.Tests.Add(test4);
            context.Tests.Add(test5);


            var question0 = new Question { Type = "radio", Text = "Question 1?", Test = test0, Score = 10};
            var option01 = new Option { Question = question0, Text = "Option1", IsAnswer = true };
            var option02 = new Option { Question = question0, Text = "Option2", IsAnswer = false };
            var option03 = new Option { Question = question0, Text = "Option3", IsAnswer = false };
            context.Options.Add(option01);
            context.Options.Add(option02);
            context.Options.Add(option03);

            var question3 = new Question { Type = "radio", Text = "Question 2?",  Test = test0, Score = 10 };
            var option31 = new Option { Question = question3, Text = "Option1", IsAnswer = true };
            var option32 = new Option { Question = question3, Text = "Option2", IsAnswer = false };
            var option33 = new Option { Question = question3, Text = "Option3", IsAnswer = false };
            context.Options.Add(option31);
            context.Options.Add(option32);
            context.Options.Add(option33);

            var question1 = new Question { Type = "checkbox", Text = "Question 3?", Test = test0, Score = 10 };
            var option11 = new Option { Question = question1, Text = "Option1", IsAnswer = true };
            var option12 = new Option { Question = question1, Text = "Option2", IsAnswer = true };
            var option13 = new Option { Question = question1, Text = "Option3", IsAnswer = false };
            context.Options.Add(option11);
            context.Options.Add(option12);
            context.Options.Add(option13);

            var question2 = new Question { Type = "text", Text = "Question 4?", Test = test0, Score = 30 };
            var option21 = new Option { Question = question2, Text = "Text Answer", IsAnswer = true };
            context.Options.Add(option21);

            context.Questions.Add(question0);
            context.Questions.Add(question1);
            context.Questions.Add(question2);
            context.Questions.Add(question3);

            var question4 = new Question { Type = "radio", Text = "Question 1?", Test = test1, Score = 10 };
            var option41 = new Option { Question = question4, Text = "Option1", IsAnswer = true };
            var option42 = new Option { Question = question4, Text = "Option2", IsAnswer = false };
            var option43 = new Option { Question = question4, Text = "Option3", IsAnswer = false };
            context.Options.Add(option41);
            context.Options.Add(option42);
            context.Options.Add(option43);

            var question5 = new Question { Type = "radio", Text = "Question 1?", Test = test2, Score = 10 };
            var option51 = new Option { Question = question5, Text = "Option1", IsAnswer = true };
            var option52 = new Option { Question = question5, Text = "Option2", IsAnswer = false };
            var option53 = new Option { Question = question5, Text = "Option3", IsAnswer = false };
            context.Options.Add(option51);
            context.Options.Add(option52);
            context.Options.Add(option53);

            var question6 = new Question { Type = "radio", Text = "Question 1?", Test = test3, Score = 10 };
            var option61 = new Option { Question = question6, Text = "Option1", IsAnswer = true };
            var option62 = new Option { Question = question6, Text = "Option2", IsAnswer = false };
            var option63 = new Option { Question = question6, Text = "Option3", IsAnswer = false };
            context.Options.Add(option61);
            context.Options.Add(option62);
            context.Options.Add(option63);

            var question7 = new Question { Type = "radio", Text = "Question 1?", Test = test4, Score = 10 };
            var option71 = new Option { Question = question7, Text = "Option1", IsAnswer = true };
            var option72 = new Option { Question = question7, Text = "Option2", IsAnswer = false };
            var option73 = new Option { Question = question7, Text = "Option3", IsAnswer = false };
            context.Options.Add(option71);
            context.Options.Add(option72);
            context.Options.Add(option73);

            var question8 = new Question { Type = "radio", Text = "Question 1?", Test = test5, Score = 10 };
            var option81 = new Option { Question = question8, Text = "Option1", IsAnswer = true };
            var option82 = new Option { Question = question8, Text = "Option2", IsAnswer = false };
            var option83 = new Option { Question = question8, Text = "Option3", IsAnswer = false };
            context.Options.Add(option81);
            context.Options.Add(option82);
            context.Options.Add(option83);

            context.Questions.Add(question4);
            context.Questions.Add(question5);
            context.Questions.Add(question6);
            context.Questions.Add(question7);
            context.Questions.Add(question8);

            base.Seed(context);
        }
    }
}
